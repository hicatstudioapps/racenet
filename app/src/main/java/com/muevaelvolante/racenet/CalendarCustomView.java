package com.muevaelvolante.racenet;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Collector;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.adapters.calendar.GridAdapter;
import com.muevaelvolante.racenet.fragment.race_agenda.RaceAgendaCalendarFragment;
import com.muevaelvolante.racenet.fragment.race_agenda.RaceAgendaFragment;
import com.muevaelvolante.racenet.model.race_agenda.Agenda;
import com.muevaelvolante.racenet.model.race_agenda.DayEvent;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.stream.Collectors;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by admin on 13/04/2017.
 */

public class CalendarCustomView extends LinearLayout {
    private static final String TAG = CalendarCustomView.class.getSimpleName();
    @Bind(R.id.mon)
    TextView mon;
    @Bind(R.id.tue)
    TextView tue;
    @Bind(R.id.wed)
    TextView wed;
    @Bind(R.id.thu)
    TextView thu;
    @Bind(R.id.fri)
    TextView fri;
    @Bind(R.id.sat)
    TextView sat;
    @Bind(R.id.sun)
    TextView sun;
    @Bind(R.id.display_current_date)
    TextView displayCurrentDate;
    private TextView currentDate;
    private GridView calendarGridView;
    private static final int MAX_CALENDAR_COLUMN = 42;
    private int month, year;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM", Locale.ENGLISH);
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    private Context context;
    private GridAdapter mAdapter;
    private List<Agenda> agenda;
    private RaceAgendaCalendarFragment listener;
    LinkedTreeMap<Integer,ArrayList<Agenda>> treeMap= new LinkedTreeMap<>();

    public CalendarCustomView(Context context) {
        super(context);
    }

    public CalendarCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        initializeUILayout();
        Log.d(TAG, "I need to call this method");
    }

    public void init(Calendar calendar){
        this.cal=calendar;
        setUpCalendarAdapter();
        setGridCellClickEvents();
    }

    public CalendarCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initializeUILayout() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_layout, this);
        ButterKnife.bind(this,view);
        currentDate = (TextView) view.findViewById(R.id.display_current_date);
        currentDate.setTypeface(Aplication.hregular);
        mon.setTypeface(Aplication.hregular);
        tue.setTypeface(Aplication.hregular);
        wed.setTypeface(Aplication.hregular);
        thu.setTypeface(Aplication.hregular);
        fri.setTypeface(Aplication.hregular);
        sat.setTypeface(Aplication.hregular);
        sun.setTypeface(Aplication.hregular);
        calendarGridView = (GridView) view.findViewById(R.id.calendar_grid);
    }

    private void setGridCellClickEvents() {
        calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DayEvent dayEvent= (DayEvent) view.getTag();
                if(dayEvent.isEvent()>=1)
                listener.goToRaceAgenda(treeMap,dayEvent.getAdapterPosition());
            }
        });
    }

    private void setUpCalendarAdapter() {
        //cargamos en background los dias que hay eventos
        rx.Observable.create(new rx.Observable.OnSubscribe<List<DayEvent>>() {
            @Override
            public void call(Subscriber<? super List<DayEvent>> subscriber) {
                List<DayEvent> dayValueInCells = new ArrayList<>();
                Calendar mCal = (Calendar) cal.clone();
                Log.d(TAG, new DateTime(mCal).toString("dd-MM"));
                mCal.set(Calendar.DAY_OF_MONTH, 1);
                Log.d(TAG, new DateTime(mCal).toString("dd-MM"));
                int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) +5;
                mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
                // generamos la lista de eventos x dias, para el viewpager<posicion,eventos>
                int position=0;
                Log.d(TAG, new DateTime(mCal).toString("dd-MM"));
                while (dayValueInCells.size() < MAX_CALENDAR_COLUMN) {
                    DayEvent dayEvent=new DayEvent(mCal.getTime(),findEventDay(mCal.getTime()));
                    dayValueInCells.add(dayEvent);
                    if(dayEvent.isEvent()>= 1){// si eventos para ese dia, los guardamos todos
                        dayEvent.setAdapterPosition(position);// guardamos posicion en el viewpager para ese dia
                        treeMap.put(position++, getEventForDay(mCal.getTime()));
                    }
                    mCal.add(Calendar.DAY_OF_MONTH, 1);
                }
                subscriber.onNext(dayValueInCells);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<DayEvent>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<DayEvent> dayEvents) {
                        String sDate = formatter.format(cal.getTime());
                        currentDate.setText(sDate);
                        mAdapter = new GridAdapter(context, dayEvents, cal);
                        calendarGridView.setAdapter(mAdapter);// caramos adapter calendar
                        listener.initRaceAgenda(treeMap); //creamos el fragment race-agenda
                        listener.hideDialog();
                    }
                });


    }

    private ArrayList<Agenda> getEventForDay(Date time) {
        DateTime reference= new DateTime(time);
        ArrayList<Agenda> result= new ArrayList<>();
        result.addAll(Stream.of(agenda).filter(agenda1 -> {
            if(TextUtils.isEmpty(agenda1.getDate()))
                return false;
            DateTime date=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withLocale(Locale.ENGLISH).parseDateTime(agenda1.getDate());
            if(reference.getDayOfMonth()== date.getDayOfMonth() && reference.getMonthOfYear()== date.getMonthOfYear())
                return true;
            return false;
        }).collect(com.annimon.stream.Collectors.toList()));
        return result;
    }

    public void setAgenda(List<Agenda> agenda) {
        this.agenda = agenda;
    }

    private int findEventDay(Date day){
        DateTime reference= new DateTime(day);
        Optional<Agenda> result= Stream.of(agenda).filter(agenda1 -> {
            if(TextUtils.isEmpty(agenda1.getDate()))
                return false;
            DateTime date=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withLocale(Locale.ENGLISH).parseDateTime(agenda1.getDate());
            if(reference.getDayOfMonth()== date.getDayOfMonth() && reference.getMonthOfYear()== date.getMonthOfYear())
                return true;
            return false;
        }).findFirst();
        if(result.isPresent()){
            if(result.get().getTitle().toLowerCase().contains("no official racing schedule"))
                return 1;
            return 2;
        }
        return 0;
    }

    public void setListener(RaceAgendaCalendarFragment raceAgendaCalendarFragment) {
        this.listener= raceAgendaCalendarFragment;
    }

    class D extends Date{

    }
}
