package com.muevaelvolante.racenet.activity;

import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.ServiceCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.model.agenda.CityMap;
import com.muevaelvolante.racenet.model.agenda.VenueMap;
import com.muevaelvolante.racenet.service.ServiceGenerator;
import com.rey.material.widget.ProgressView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class EventMapActivity extends AppCompatActivity implements OnMapReadyCallback,OnLoadCompleteListener {

    private GoogleMap mMap;
    String selected_tabColor = "#ffffff";
    String unSelected_tabColor = "#64c5c5c8";
    private MaterialDialog mProgressDialog;
    List<CityMap> cityMaps;
    VenueMap venue_map;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toobar_title;
    @Bind(R.id.loading)
    ProgressView progressView;
    @Bind(R.id.progress_text)
    TextView progress_text;
    @Bind(R.id.pdfView)
    PDFView pdfView;
    @Bind(R.id.map_container)
    View map_container;
    @Bind(R.id.textView10)
    TextView veneue_txt;
    @Bind(R.id.textView9)
    TextView map_txt;
    @Bind(R.id.imageView5)
            View venue_indicator;
    @Bind(R.id.imageView6)
            View map_indicator;
    DonwLoadPdf donwLoadPdf;
    float zoomLevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_map_activity);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        toobar_title.setTypeface(Aplication.hmedium);
        cityMaps = (List<CityMap>) getIntent().getSerializableExtra("d");
        venue_map = (VenueMap) getIntent().getSerializableExtra("dd");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        showProgressDialog("");
        donwLoadPdf = new DonwLoadPdf();
        donwLoadPdf.execute(new String[]{venue_map.getMedia().get(0).getFile()});
    }

    @OnClick(R.id.root_indicator2)
    public void veneueClick() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_in_forward);
        map_container.setAnimation(anim);
        map_container.setVisibility(View.VISIBLE);
        veneue_txt.setTextColor(Color.parseColor(unSelected_tabColor));
        map_txt.setTextColor(Color.parseColor(selected_tabColor));
        map_indicator.setVisibility(View.VISIBLE);
        venue_indicator.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.root_indicator1)
    public void mapClick() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_out_forward);
        map_container.setAnimation(anim);
        map_container.setVisibility(View.GONE);
        map_txt.setTextColor(Color.parseColor(unSelected_tabColor));
        veneue_txt.setTextColor(Color.parseColor(selected_tabColor));
        map_indicator.setVisibility(View.INVISIBLE);
        venue_indicator.setVisibility(View.VISIBLE);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        zoomLevel= mMap.getMaxZoomLevel();
        for (CityMap map :
                cityMaps) {
            LatLng latLng = new LatLng(Double.parseDouble(map.getLatitude()), Double.parseDouble(map.getLongitude()));
            Log.d("coord",latLng.latitude+" "+latLng.longitude);
            mMap.addMarker(new MarkerOptions().position(latLng).title(map.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(cityMaps.get(0).getLatitude()), Double.parseDouble(cityMaps.get(0).getLongitude())), (float) (zoomLevel*0.7)));
        progressView.start();
        hideDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    public void showProgressDialog(String text) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .content("Loading...")
                .progress(true, 0)
                .widgetColor(Color.parseColor("#454553"))
                .cancelable(false)
                .build();
        mProgressDialog.show();
    }

    public void hideDialog() {
        mProgressDialog.dismiss();
    }

    @Override
    public void loadComplete(int nbPages) {
        progressView.stop();
        progress_text.setVisibility(View.GONE);
    }

    class DonwLoadPdf extends AsyncTask<String, Integer, Uri> {

        @Override
        protected Uri doInBackground(String... params) {
            ISettings settings = ServiceGenerator.createService(ISettings.class);
            Call<ResponseBody> fileStream = settings.downloadFilePdf(params[0]);
            try {
                ResponseBody body = fileStream.execute().body();
                File outFile = new File(Constants.appRootDir, "temp.pdf");
                int fileLength = (int) body.contentLength();
                InputStream input = new BufferedInputStream(body.byteStream());
                OutputStream output = null;
                output = new FileOutputStream(outFile);
                byte data[] = new byte[1024];
                long total = 0;
                int count = 0;
                while ((count = (input.read(data))) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    publishProgress(Integer.valueOf("" + (int) (total * 100 / fileLength)));
                }
                output.flush();
                output.close();
                input.close();
                return Uri.parse(outFile.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            if (uri == null) {
                progress_text.setText("Error loading file..");
                progressView.stop();
            } else {
                progress_text.setText("Opening file...");
                pdfView.fromUri(uri)
                        .enableAnnotationRendering(true)
                        .onLoad(EventMapActivity.this)
                        .load();
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progress_text.setText(values[0] + "%");
        }
    }
}
