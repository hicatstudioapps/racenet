package com.muevaelvolante.racenet.activity;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.NewsItemFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsDetailActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toobar_title;
    @Bind(R.id.pager)
    ViewPager pager;
    private AdapterNewsPager mAdapterNewsPager;
    boolean conference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        toobar_title.setTypeface(Aplication.hmedium);
        conference = getIntent().getExtras().getBoolean("conference");
        initViewPager(getIntent().getIntExtra("pos", 0));

    }

    @Override
    protected void onResume() {
        super.onResume();
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("News detail");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    /**
     * Método para la inicialización del ViewPager.
     *
     * @param position
     */
    private void initViewPager(int position) {

        this.mAdapterNewsPager = new AdapterNewsPager(getSupportFragmentManager(), conference ? Aplication.newsList.getConferenceNews() :
                Aplication.newsList.getRacenetNews());

     pager.setAdapter(mAdapterNewsPager);
     pager.setCurrentItem(position);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    public class AdapterNewsPager extends FragmentPagerAdapter {

        /*
         * Objeto usuario.
         */
        List<Object> mFeeds;


        /**
         * Constructor por defecto.
         *
         * @param fm
         * @param feeds
         */
        public AdapterNewsPager(android.support.v4.app.FragmentManager fm, ArrayList feeds) {

            super(fm);
            this.mFeeds = feeds;

        }


        @Override
        public android.support.v4.app.Fragment getItem(int pos) {

			/*
			 * Todas las pantallas son idénticas, siendo asociada cada una con un objeto Feed.
			 */
            android.support.v4.app.Fragment fragment = NewsItemFragment.newInstance(this.mFeeds.get(pos));

            return fragment;

        }


        @Override
        public int getCount() {

            return this.mFeeds.size();

        }
    }
}
