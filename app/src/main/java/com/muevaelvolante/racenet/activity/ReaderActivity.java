package com.muevaelvolante.racenet.activity;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.service.ServiceGenerator;
import com.rey.material.widget.ProgressView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ReaderActivity extends AppCompatActivity implements OnLoadCompleteListener {

    @Bind(R.id.loading)
    ProgressView progressView;
    @Bind(R.id.progress_text)
    TextView progress_text;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.pdfView)
    PDFView pdfView;
    boolean fileReady=false;
    String url;
    DonwLoadPdf donwLoadPdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        url= getIntent().getStringExtra("url");
        donwLoadPdf= new DonwLoadPdf();
//        donwLoadPdf.execute("http://192.168.111.1:8081/race/m43609_payslip-instructions.pdf");
        donwLoadPdf.execute(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_empty,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home)
            finish();
        else if(item.getItemId()== R.id.down){
            if(donwLoadPdf.getStatus()== AsyncTask.Status.RUNNING){
                Toast.makeText(this,"File not ready yet.",Toast.LENGTH_SHORT).show();
            return true;}
            if(fileReady){
                File dir=new File(Constants.appRootDir+File.separator+"PDFs");
                if(!dir.exists())
                    dir.mkdir();
                File outFile = new File(Constants.appRootDir+File.separator, "temp.pdf");
                if (outFile.exists()) {
                    boolean result = outFile.renameTo(new File(Constants.appRootDir + File.separator + "PDFs" + File.separator + System.currentTimeMillis() + ".pdf"));
                    if(result)
                        Toast.makeText(this,"File saved in " + outFile.getAbsolutePath(),Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this,"Error saving file",Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    class DonwLoadPdf extends AsyncTask<String, Integer, Uri> {

        @Override
        protected Uri doInBackground(String... params) {
            ISettings settings = ServiceGenerator.createService(ISettings.class);
            Call<ResponseBody> fileStream = settings.downloadFilePdf(params[0]);
            try {
                ResponseBody body = fileStream.execute().body();
                File outFile = new File(Constants.appRootDir+File.separator, "temp.pdf");
                int fileLength = (int) body.contentLength();
                InputStream input = new BufferedInputStream(body.byteStream());
                OutputStream output = null;
                output = new FileOutputStream(outFile);
                byte data[] = new byte[1024];
                long total = 0;
                int count = 0;
                while ((count = (input.read(data))) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    publishProgress(Integer.valueOf("" + (int) (total * 100 / fileLength)));
                }
                output.flush();
                output.close();
                input.close();
                return Uri.parse(outFile.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.start();
        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            if (uri == null) {
                progress_text.setText("Error loading file..");
                progressView.stop();
            } else {
                progress_text.setText("Opening file...");
                pdfView.fromUri(uri)
                        .enableAnnotationRendering(true)
                        .onLoad(ReaderActivity.this)
                        .swipeHorizontal(true)
                        .load();
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progress_text.setText(values[0] + "%");
        }
    }

    @Override
    public void loadComplete(int nbPages) {
        progressView.stop();
        progress_text.setVisibility(View.GONE);
        fileReady=true;
    }
}
