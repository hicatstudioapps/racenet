package com.muevaelvolante.racenet.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.RouteDetailFragment;
import com.muevaelvolante.racenet.model.route.Stopover;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RouteDetailActivity extends AppCompatActivity {

    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toobar_title;
    private RouteAdapter mRouteAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        toobar_title.setTypeface(Aplication.hmedium);
        initViewPager(getIntent().getExtras().getInt("pos"));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Aplication application = (Aplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Route detail");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initViewPager(int position) {

        this.mRouteAdapter = new RouteAdapter(getSupportFragmentManager(), Aplication.routeList);
        pager.setAdapter(mRouteAdapter);
        pager.setCurrentItem(position);

    }
    public class RouteAdapter extends FragmentPagerAdapter {

        /*
         * Objeto usuario.
         */
        List<Stopover> mFeeds;

        /**
         * Constructor por defecto.
         *
         * @param fm
         * @param feeds
         */
        public RouteAdapter(android.support.v4.app.FragmentManager fm, List feeds) {

            super(fm);
            this.mFeeds = feeds;

        }


        @Override
        public android.support.v4.app.Fragment getItem(int pos) {

			/*
			 * Todas las pantallas son idénticas, siendo asociada cada una con un objeto Feed.
			 */
            android.support.v4.app.Fragment fragment = RouteDetailFragment.newInstance(this.mFeeds.get(pos),"");

            return fragment;

        }


        @Override
        public int getCount() {

            return this.mFeeds.size();

        }
    }
}

