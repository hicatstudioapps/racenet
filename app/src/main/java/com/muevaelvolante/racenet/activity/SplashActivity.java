package com.muevaelvolante.racenet.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.interfaces.ILoginWeb;
import com.muevaelvolante.racenet.model.web.ApiWeb;
import com.muevaelvolante.racenet.service.ServiceGenerator;
import com.muevaelvolante.racenet.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class SplashActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    private static final int RC_SIGN_IN = 2303;
    @Bind(R.id.textView)
    TextView bottom_text1;
    @Bind(R.id.textView2)
    TextView bottom_text2;
    @Bind(R.id.textView3)
    TextView forgot;
    @Bind(R.id.textView4)
    TextView welcome;
    @Bind(R.id.username)
    EditText username;
    @Bind(R.id.password)
    TextView password;
    @Bind(R.id.textView5)
    TextView staf;
    @Bind(R.id.textView6)
    TextView login;
    private MaterialDialog mProgressDialog;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!getSharedPreferences("login",MODE_PRIVATE).getBoolean("isLoged",false)){
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Aplication.screenW = (Utils.getScreenWidth(this));
        bottom_text1.setTypeface(Aplication.llight);
        bottom_text2.setTypeface(Aplication.llight);
        forgot.setTypeface(Aplication.llight);
        username.setTypeface(Aplication.hlight);
        password.setTypeface(Aplication.hlight);
        welcome.setTypeface(Aplication.hlight);
        staf.setTypeface(Aplication.hmedium);
        login.setTypeface(Aplication.hmedium);
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
            findViewById(R.id.textView2).setOnClickListener(view -> Utils.sendMailTo(SplashActivity.this,"racenet.support@volvooceanrace.com","",""));
            findViewById(R.id.textView3).setOnClickListener(view -> {
                Intent intent= new Intent(Intent.ACTION_VIEW,Uri.parse("http://racenet.volvooceanrace.com/en/forgot-password"));
                startActivity(Intent.createChooser(intent,""));
            });
        }
        else{
            Intent home= new Intent(SplashActivity.this, HomeActivity.class);
            if(getIntent().getExtras()!=null){
                home.putExtras(getIntent().getExtras());
            }
            startActivity(home);
            finish();
        }
       }

    public static Drawable changeDrawableColor(int drawableRes, int colorRes, Context context) {
        //Convert drawable res to bitmap
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), drawableRes);
        final Bitmap resultBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth() - 1, bitmap.getHeight() - 1);
        final Paint p = new Paint();
        final Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);

        //Create new drawable based on bitmap
        final Drawable drawable = new BitmapDrawable(context.getResources(), resultBitmap);
        drawable.setColorFilter(new
                PorterDuffColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY));
        return drawable;
    }

    @OnClick(R.id.login)
    public void login(View view) {
        showProgressDialog("Login...");
        new WebApiLogin().execute(new String[]{username.getText().toString(), password.getText().toString()});
    }

    private void showProgressDialog(String text) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .content(text)
                .progress(true, 0)
                .widgetColor(Color.parseColor("#454553"))
                .cancelable(false)
                .build();
        mProgressDialog.show();
    }

    private void showErrorDialog(String text) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .title("Login Error")
                .content(text)
                .widgetColor(Color.parseColor("#454553"))
                .positiveColor(Color.parseColor("#454553"))
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this,"Error connecting to login server",Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.staaf_login)
    public void signIn() {
        showProgressDialog("Login...");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String email=acct.getEmail();
            //email="albano@volvooceanrace.com";
            if(email.contains("muevaelvolante.com") || email.contains("volvo") || email.contains("qtjambiii"))
                updateUI(true);
            else
                updateUI(false);
          } else {
            hideProgressDialog();
            showErrorDialog("Error connecting whith server.");
        }
    }

    private void updateUI(boolean b) {
        hideProgressDialog();
        if(b){
            getSharedPreferences("login",MODE_PRIVATE).edit().putBoolean("isLoged",true).commit();
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        }else{
            showErrorDialog("Only @volvooceanrace.com accounts are supported for staff login.");
        }
    }

    class WebApiLogin extends AsyncTask<String, Void, ApiWeb> {

        @Override
        protected void onPostExecute(ApiWeb apiWeb) {
            super.onPostExecute(apiWeb);
            if (apiWeb == null) {
                hideProgressDialog();
                showErrorDialog("Error getting response from server.");
            } else {
                if (apiWeb.getSuccess()) {
                    hideProgressDialog();
                    getSharedPreferences("login",MODE_PRIVATE).edit().putBoolean("isLoged",true).commit();
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();
                } else {
                    hideProgressDialog();
                    showErrorDialog(apiWeb.getError());
                }
            }
        }

        @Override
        protected ApiWeb doInBackground(String... params) {
            ILoginWeb loginWeb = ServiceGenerator.createService(ILoginWeb.class);
            Map<String, String> mapData = new LinkedHashMap<String, String>();
            mapData.put("username", params[0]);
            mapData.put("password", params[1]);
            Call<ApiWeb> call = loginWeb.login("http://racenet.volvooceanrace.com/api/1/login", mapData);
            try {
                ApiWeb api = call.execute().body();
                return api;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
