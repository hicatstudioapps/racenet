package com.muevaelvolante.racenet.activity;

import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.model.who.Person;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class WhoIsWhoDetailActivity extends AppCompatActivity {
    @Bind(R.id.name)
    TextView name;
    @Bind(R.id.company)
    TextView company;
    @Bind(R.id.role)
    TextView role;
    @Bind(R.id.email)
    TextView email;
    @Bind(R.id.phone)
    TextView phone;
    //@Bind(R.id.save)
    //TextView save;
    @Bind(R.id.icon)
    CircleImageView icon;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.email_container)
    RelativeLayout emailContainer;
    @Bind(R.id.phone_container)
    RelativeLayout phoneContainer;
    @Bind(R.id.save_container)
    RelativeLayout saveContainer;
    private Bitmap bitMap;
    Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        person = (Person) getIntent().getSerializableExtra("p");
        setContentView(R.layout.activity_who_is_who_detail);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_back);
        getSupportActionBar().setHomeButtonEnabled(true);
        name.setTypeface(Aplication.hlight);
        company.setTypeface(Aplication.hregular);
        role.setTypeface(Aplication.hregular);
        email.setTypeface(Aplication.hmedium);
        phone.setTypeface(Aplication.hmedium);
        //save.setTypeface(Aplication.hmedium);
        name.setText(person.getFullname());
        company.setText(person.getCompany());
        role.setText(person.getRole());
        email.setText(person.getEmail());
        emailContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.sendMailTo(WhoIsWhoDetailActivity.this, person.getEmail(), "", person.getEmail_cc());
            }
        });
        phone.setText(person.getPhoneMobile());
        String phoneNumber = person.getPhoneMobile();
        phoneNumber = phoneNumber.replace(" ", "");
        if (PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            phoneContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.callTo(WhoIsWhoDetailActivity.this, person.getPhoneMobile());
                }
            });
        }
        ImageLoader.getInstance().displayImage(person.getMedia().getMed(), icon, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                if (bitmap != null)
                    WhoIsWhoDetailActivity.this.bitMap = bitmap;
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return true;
    }

    @OnClick(R.id.save_container)
    void save() {

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactID = ops.size();
        // Adding insert operation to operations list
        // to insert a new raw contact in the table ContactsContract.RawContacts
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        // Adding insert operation to operations list
        // to insert display name in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, person.getFullname()).
                        build());

        // Adding insert operation to operations list
        // to insert Mobile Number in the table ContactsContract.Data
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, person.getPhoneMobile().toString())
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                .build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, person.getPhoneOffice().toString())
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                .build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, person.getEmail().toString())
                //.withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                .build());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitMap != null) {    // If an image is selected successfully
            bitMap.compress(Bitmap.CompressFormat.PNG, 75, stream);
            // Adding insert operation to operations list
            // to insert Photo in the table ContactsContract.Data
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, stream.toByteArray())
                    .build());

            try {
                stream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            // Executing all the insert operations as a single database transaction
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Toast.makeText(getBaseContext(), "Contact is successfully added", Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }


//    @OnClick(R.id.save_container)
//    public void save(){
//        VCard vcard = new VCard();
//        vcard.addTelephoneNumber(person.getPhoneMobile(), TelephoneType.WORK);
//        vcard.setFormattedName(person.getFullname());
//        vcard.setOrganization(person.getCompany(),person.getRole());
//        vcard.addEmail(person.getEmail(), EmailType.HOME);
//        save
//        File dir = new File(Constants.appRootDir + File.separator + "Vcard");
//        if (!dir.exists())
//            dir.mkdir();
//        File outFile = new File(Constants.appRootDir + File.separator+ "Vcard"+File.separator, person.getFirstname()+".vcf");
//        try {
//            Ezvcard.write(vcard).version(VCardVersion.V3_0).go(outFile);
//            Toast.makeText(this, "Vcard saved in " + outFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Fail saving contact ", Toast.LENGTH_SHORT).show();
//        }
//    }
}
