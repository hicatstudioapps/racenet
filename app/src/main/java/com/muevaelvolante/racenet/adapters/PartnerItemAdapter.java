package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.PartnerDetailActivity;
import com.muevaelvolante.racenet.fragment.PartnerFragment;
import com.muevaelvolante.racenet.model.partner.Partner;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 09/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class PartnerItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public String section;
    List<Object> partners;
    Context context;
    private final PartnerFragment fragment;

    public PartnerItemAdapter(List<Object> partners, Context context,String section,PartnerFragment partner) {
        this.partners = partners;
        this.context = context;
        this.section=section;
        this.fragment= partner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_partner_detail_item, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.partner_divider, parent, false);
            return new DividerViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(partners.get(position) instanceof String)
            return 0;
        else
            return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case 1:
                String url;
                Partner data= (Partner) partners.get(position);
                if (Aplication.is_teblet) {
                    url = ((LinkedTreeMap<String, String>) (data).getMedia().get(0)).get("hi");
                } else {
                    url = ((LinkedTreeMap<String, String>) data.getMedia().get(0)).get("med");
                }
                ImageLoader.getInstance().displayImage(url, ((ViewHolder)holder).image, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        Log.d("", "onLoadingComplete: ");
                        float proportion = (float) bitmap.getWidth() / bitmap.getHeight();
                        int alto = (int) (Aplication.screenW / proportion);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        lp.height = alto;
                        lp.width = Aplication.screenW;
                        view.setLayoutParams(lp);
                        //((RelativeLayout)view.getParent()).setLayoutParams(lp);
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
                break;
            case 0:
                ((DividerViewHolder)holder).partner_selected.setText(partners.get(position).toString());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return partners.size();
    }

    String getPartnerCat(int position){
        for (int i=position-1; i>=0;i--)
            if(partners.get(i) instanceof String)
                return (String) partners.get(i);
        return "";
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.imageView9)
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Bitmap b = BitmapFactory.decodeResource(context.getResources(), R.drawable.generic_image_rectangular);
            float proportion = (float) b.getWidth() / b.getHeight();
            int alto = (int) ((Aplication.screenW) / proportion);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) image.getLayoutParams();
            lp.height = alto;
            lp.width = Aplication.screenW;
            image.setLayoutParams(lp);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent detail = new Intent(context, PartnerDetailActivity.class);
                    Bundle data = new Bundle();
                    Aplication.partnerList.clear();
                    Aplication.partnerList.addAll(partners);
                    ArrayList<Integer> remove = new ArrayList();
                    int decrease=0; //me dice cuando tengo que quitar a la posicion original para mapear la posicion en el array resultante
                    for (int i = Aplication.partnerList.size()-1; i>= 0; i--) {
                        if (Aplication.partnerList.get(i) instanceof String) {
                            remove.add(i);
                            if(i<getAdapterPosition())
                                decrease++;
                        }
                    }
                    if (remove.size()>0)
                        for (int in :
                                remove) {
                            Aplication.partnerList.remove(in);
                        }
                    data.putInt("pos", getAdapterPosition() - decrease);
                    detail.putExtras(data);
                    context.startActivity(detail);
                }
            });
        }
    }

    public class DividerViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.textView18)
        TextView partner_selected;

        public DividerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            partner_selected.setTypeface(Aplication.hlight);
        }
    }
}
