package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.model.race_agenda.Agenda;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jorgedavid on 18/4/17.
 */

public class RaceAgendaAdapter extends RecyclerView.Adapter<RaceAgendaAdapter.ViewHolder> {

    private final Context context;
    List<Agenda> agendas;
    List<Agenda> current;

    public void setData(List<Agenda> current) {
        this.current = current;
    }

    public RaceAgendaAdapter(List<Agenda> agendas,Context context) {
        this.agendas = agendas;
        this.current= agendas;
        this.context=context;
    }
    
    public void reset(){
        this.current=this.agendas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_race_agenda_day_item, null);
        view.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewHolder holder= new ViewHolder(view);
        holder.content.setLayoutParams(new LinearLayout.LayoutParams((int) (Aplication.screenW*0.7), ViewGroup.LayoutParams.WRAP_CONTENT));
        LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams((int) (Aplication.screenW*0.3), ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity= Gravity.CENTER_VERTICAL;
        holder.time.setLayoutParams(lp);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.time.setVisibility(View.INVISIBLE);
        holder.time.setText("22:00");
        if(TextUtils.isEmpty(current.get(position).getBigger_font()))
            holder.title.setTextSize(TypedValue.COMPLEX_UNIT_PX,context.getResources().getDimension(R.dimen.biger_font_normal));
        else
            holder.title.setTextSize(TypedValue.COMPLEX_UNIT_PX,context.getResources().getDimension(R.dimen.biger_font));
        if(TextUtils.isEmpty(current.get(position).getTime())){
            holder.time.setVisibility(View.INVISIBLE);
        }
        else{
            holder.time.setText(current.get(position).getTime());
            holder.time.setVisibility(View.VISIBLE);
        }
        holder.title.setText(current.get(position).getTitle());
        holder.text.setText(current.get(position).getSubtitle());
    }

    @Override
    public int getItemCount() {
        return current.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.time)
        TextView time;
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.textView19)
        TextView text;
        @Bind(R.id.content)
        LinearLayout content;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            time.setTypeface(Aplication.hregular);
            title.setTypeface(Aplication.hregular);
            text.setTypeface(Aplication.hregular);
        }
    }
}
