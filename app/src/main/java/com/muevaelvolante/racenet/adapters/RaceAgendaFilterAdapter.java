package com.muevaelvolante.racenet.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.race_agenda.RaceAgendaFragment;
import com.muevaelvolante.racenet.model.race_agenda.Filter_;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jorgedavid on 19/4/17.
 */

public class RaceAgendaFilterAdapter extends RecyclerView.Adapter<RaceAgendaFilterAdapter.ViewHolder> {

    List<Filter_> filters;
    RaceAgendaFragment listaner;

    public RaceAgendaFilterAdapter(List<Filter_> filters, RaceAgendaFragment listaner) {
        this.filters = filters;
        this.listaner = listaner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View holder= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_partner_item,null);
        holder.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(holder);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0)
            holder.partner.setText("ALL FILTERS");
        else
            holder.partner.setText(filters.get(position - 1).getFilter());
    }

    @Override
    public int getItemCount() {
        return filters.size() +1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.partner_text)
        TextView partner;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            partner.setTypeface(Aplication.hlight);
            itemView.setOnClickListener(view ->{
                    if(getAdapterPosition()==0)
                        listaner.filter("all");
                else
            listaner.filter(filters.get(getAdapterPosition()-1).getFilter());
            });

        }
    }
}
