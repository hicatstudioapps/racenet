package com.muevaelvolante.racenet.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.race_agenda.RaceAgendaCalendarFragment;
import com.muevaelvolante.racenet.fragment.race_agenda.RaceScheduleFragmentNew;
import com.muevaelvolante.racenet.model.race_agenda.list.Agenda;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jorgedavid on 13/4/17.
 */

public class RaceAgendaRoutesAdapter extends RecyclerView.Adapter<RaceAgendaRoutesAdapter.ViewHolder> {

    private final RaceScheduleFragmentNew raceAgenda;
    List<Agenda> agendaList;

    public RaceAgendaRoutesAdapter(List<Agenda> agendaList, RaceScheduleFragmentNew raceAgenda) {
        this.agendaList = agendaList;
        this.raceAgenda= raceAgenda;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.race_agenda_item, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView21.setTypeface(Aplication.hthin);
        holder.textView21.setText(agendaList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return agendaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageView19)
        ImageView imageView19;
        @Bind(R.id.textView21)
        TextView textView21;

       public ViewHolder(View view) {
           super(view);
            ButterKnife.bind(this, view);
           view.setOnClickListener(view1 -> {
               raceAgenda.goTo(RaceAgendaCalendarFragment.newInstance(agendaList.get(getAdapterPosition()).getUrl(),
                       agendaList.get(getAdapterPosition()).getName()));
           });
        }
    }


}
