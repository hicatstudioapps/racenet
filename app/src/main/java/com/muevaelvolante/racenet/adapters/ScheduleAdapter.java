package com.muevaelvolante.racenet.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.SpeakerDetailActivity;
import com.muevaelvolante.racenet.model.agenda.Agenda;
import com.muevaelvolante.racenet.model.agenda.Day;
import com.muevaelvolante.racenet.model.agenda.Speaker;
import com.muevaelvolante.racenet.util.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 23/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> implements
        HorizontalDividerItemDecoration.MarginProvider{


    int viewType; //0-day1, 1-day2, 2-filter,3-day3
    Context context;
    Agenda agenda;
    ArrayList<Integer> procesed= new ArrayList<>();
    int serachTarget;

    public int getSerachTarget() {
        return serachTarget;
    }

    public void setSerachTarget(int serachTarget) {
        this.serachTarget = serachTarget;
    }

    private List<Day> data;

    public List<Day> getData() {
        return data;
    }

    public void setData(List<Day> data) {
        this.data = data;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public void resetProcesed(){
        procesed= new ArrayList<>();
    }

    public int getViewType() {
        return viewType;
    }

    public ScheduleAdapter(int viewType, Context context, Agenda agenda) {
        this.viewType = viewType;
        this.context = context;
        this.agenda = agenda;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(((LayoutInflater) Aplication.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_schedule_item, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Day day= data.get(position);
        Day last=null;
        if (position!=0)
            last= data.get(position-1);
        holder.title.setText(day.getTitle());
        DateTime dateTime= Utils.convertStringToDate2(day.getDate());
        String currentDte=dateTime.toString("HH:mm");
        if(last!=null && Utils.convertStringToDate2(last.getDate()).toString("HH:mm").equals(currentDte)){
            holder.time.setVisibility(View.INVISIBLE);
        }
        else
        {
            holder.time.setText(currentDte);
            holder.time.setVisibility(View.VISIBLE);
        }
        holder.text.setText(Html.fromHtml(day.getText().get(0)));
        holder.room.setText(day.getRoom());
        holder.time.measure(0,0);
        int width= holder.time.getMeasuredWidth();
        LinearLayout.LayoutParams lp= new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int margin= (int) (context.getResources().getDimensionPixelOffset(R.dimen.schedule_item_date_margin)*2+
                        width);
        lp.setMargins(margin,0,0,0);
        holder.text.setLayoutParams(lp);
        LinearLayout.LayoutParams lp2= (LinearLayout.LayoutParams) holder.speaker_container.getLayoutParams();
        lp2.setMargins(margin,0,lp2.rightMargin,0);
        holder.speaker_container.setLayoutParams(lp2);
        holder.text.setVisibility(View.GONE);
        holder.speaker_container.setVisibility(View.GONE);
        holder.close.setVisibility(View.GONE);
        holder.speaker_container1.setVisibility(View.INVISIBLE);
//        if(!day.getSpeaker().equals("0")){
//            try {
//
//
//            Speaker speaker= agenda.findSpeakerById(day.getSpeaker());
////            holder.speaker_company_plus_role.setText(speaker.getCompany()+" "+ speaker.getRole());
//            holder.speaker_name1.setText(speaker.getFullname());
//            holder.speaker_name.setText(speaker.getFullname());
//            ImageLoader.getInstance().displayImage(speaker.getMedia().get(0).getMed(),holder.speaker_icon);
//            ImageLoader.getInstance().displayImage(speaker.getMedia().get(0).getMed(),holder.speaker_icon1);
//            holder.speaker_container1.setVisibility(View.VISIBLE);
//            }catch (Exception e){
//                holder.speaker_container1.setVisibility(View.INVISIBLE);
//            }
//        }
//        if(procesed.contains(position)){
//            holder.root.setBackgroundColor(Color.parseColor("#f7f7f7"));
//            holder.speaker_container1.setVisibility(View.INVISIBLE);
//            holder.close.setVisibility(View.VISIBLE);
//            if(!TextUtils.isEmpty(day.getText().get(0)))
//                holder.text.setVisibility(View.VISIBLE);
//            if(!day.getSpeaker().equals("0")){
//                holder.speaker_container.setVisibility(View.VISIBLE);}
//        }else
//            holder.root.setBackgroundColor(Color.parseColor("#ffffff"));

    }

    @Override
    public int getItemCount() {
        switch (viewType){
            case 0:{
                data= agenda.getDay1();
                return agenda.getDay1().size();}
            case 1:{
                data= agenda.getDay2();
                return agenda.getDay2().size();}
            case 2:
                return data.size();
            case 3:{
                data= agenda.getDay3();
                return agenda.getDay3().size();}
        }
        return 20;
    }

    @Override
    public int dividerLeftMargin(int position, RecyclerView parent) {
        return 0;
    }

    @Override
    public int dividerRightMargin(int position, RecyclerView parent) {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.time)
        TextView time;
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.textView19)
        TextView room;
        @Bind(R.id.text)
        TextView text;
        @Bind(R.id.speaker_icon)
        ImageView speaker_icon;
        @Bind(R.id.speaker_name)
        TextView speaker_name;
        @Bind(R.id.speaker_icon1)
        ImageView speaker_icon1;
        @Bind(R.id.speaker_name1)
        TextView speaker_name1;
        @Bind(R.id.speaker_company_plus_role)
        TextView speaker_company_plus_role;
        @Bind(R.id.speaker_container)
        View speaker_container;
        @Bind(R.id.speaker1_container)
        View speaker_container1;
        @Bind(R.id.close)
        View close;
        View root;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            root=itemView;
            time.setTypeface(Aplication.hregular);
            title.setTypeface(Aplication.hregular);
            text.setTypeface(Aplication.hregular);
            speaker_name.setTypeface(Aplication.hregular);
            room.setTypeface(Aplication.hregular);
            speaker_company_plus_role.setTypeface(Aplication.hregular);
//            close.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int pos= procesed.indexOf(getAdapterPosition());
//                    procesed.remove(pos);
//                    text.setVisibility(View.GONE);
//                    speaker_container.setVisibility(View.GONE);
//                    close.setVisibility(View.GONE);
//                    itemView.setBackgroundColor(Color.parseColor("#ffffff"));
//                    if(!data.get(getAdapterPosition()).getSpeaker().equals("0")){
//                        speaker_container1.setVisibility(View.VISIBLE);
//                    }
//
//                }
//            });
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    boolean isText=false;
//                    boolean isSpeaker= false;
//                    if(procesed.contains(getAdapterPosition()))
//                        return;
//                    if(!TextUtils.isEmpty(data.get(getAdapterPosition()).getText().get(0))){
//                        isText=true;
//                        if(text.getVisibility()==View.VISIBLE)
//                            return;
//                        close.setVisibility(View.VISIBLE);
//                        speaker_container1.setVisibility(View.GONE);
//                        text.setVisibility(View.VISIBLE);
//                        itemView.setBackgroundColor(Color.parseColor("#f7f7f7"));
//                    }
//                    if(!data.get(getAdapterPosition()).getSpeaker().equals("0")){
//                        isSpeaker=true;
//                        close.setVisibility(View.VISIBLE);
//                        speaker_container1.setVisibility(View.GONE);
//                        speaker_container.setVisibility(View.VISIBLE);
//                    }
//                    if(isText || isSpeaker)
//                        procesed.add(getAdapterPosition());
//                    Animation texAnim= AnimationUtils.loadAnimation(context,android.R.anim.fade_in);
//                    final boolean finalIsSpeaker = isSpeaker;
//                    texAnim.setAnimationListener(new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation animation) {
//
//                        }
//
//                        @Override
//                        public void onAnimationEnd(Animation animation) {
////                           if(finalIsSpeaker){
////                            Animation texAnim= AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left);
////                            speaker_container.setAnimation(texAnim);
////                            speaker_container.setVisibility(View.VISIBLE);
////                            close.setVisibility(View.VISIBLE);
////                            }
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animation animation) {
//
//                        }
//                    });
//                    text.setAnimation(texAnim);
//                }
//            });

//            speaker_container.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent detail= new Intent(context, SpeakerDetailActivity.class);
//                    Bundle bundle= new Bundle();
//                    bundle.putSerializable("p",agenda.findSpeakerById(data.get(getAdapterPosition()).getSpeaker()));
//                    detail.putExtras(bundle);
//                    context.startActivity(detail);
//                }
//            });
        }

    }

}
