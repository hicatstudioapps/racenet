package com.muevaelvolante.racenet.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.PartnerFragment;
import com.muevaelvolante.racenet.fragment.ScheduleFragment;
import com.muevaelvolante.racenet.model.agenda.Filter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 29/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class ScheduleFilterAdapter extends RecyclerView.Adapter<ScheduleFilterAdapter.ViewHolder> {

    private final ScheduleFragment fragment;
    private int lasViewtype;

    public ScheduleFilterAdapter(ScheduleFragment fragment, List<Filter> partners) {
        this.fragment = fragment;
        this.filter = partners;
    }

    List<Filter> filter;

    @Override
    public ScheduleFilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_partner_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0)
            holder.partner.setText("ALL FILTERS");
        else
            holder.partner.setText(filter.get(position - 1).getFilter());
    }

    @Override
    public int getItemCount() {
        return filter.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.partner_text)
        TextView partner;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            partner.setTypeface(Aplication.hlight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String filter;
                    if (lasViewtype == -1 || lasViewtype!=2)
                        lasViewtype = fragment.getScheduleAdapter().getViewType();
                    if (getAdapterPosition() == 0) {
                        filter = "";
                        if (lasViewtype == -1){
                            fragment.getScheduleAdapter().setData(fragment.getAgenda().getDayFromFilter(filter,fragment.getScheduleAdapter().getSerachTarget()));
                            fragment.getScheduleAdapter().setViewType(0);}
                        else{
                            fragment.getScheduleAdapter().setData(fragment.getAgenda().getDayFromFilter(filter,fragment.getScheduleAdapter().getSerachTarget()));
                            fragment.getScheduleAdapter().setViewType(lasViewtype);}
                    } else {
                        filter = ScheduleFilterAdapter.this.filter.get(getAdapterPosition() - 1).getFilter();
                        fragment.getScheduleAdapter().setData(fragment.getAgenda().getDayFromFilter(filter, fragment.getScheduleAdapter().getSerachTarget()));
                        fragment.getScheduleAdapter().setViewType(2);
                    }
                    fragment.getScheduleAdapter().notifyDataSetChanged();
                    fragment.showFilter();
                    fragment.getQuery().setText(filter);
                }
            });
        }
    }
}
