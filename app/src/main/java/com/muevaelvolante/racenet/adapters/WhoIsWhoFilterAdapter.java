package com.muevaelvolante.racenet.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.fragment.WhoIsWhoFragment;
import com.muevaelvolante.racenet.model.who.People;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 11/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class WhoIsWhoFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final WhoIsWhoFragment fragment;
    ArrayList<String> data;
    boolean category;
    boolean sub_category;
    People people;
    boolean name;
    String last_category;

    public WhoIsWhoFilterAdapter(People people, WhoIsWhoFragment fragment, ArrayList<String> data, boolean category, boolean sub_category, boolean name) {
        this.data = data;
        this.category = category;
        this.sub_category = sub_category;
        this.name = name;
        this.people=people;
        this.fragment=fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_who_is_who_filter_header, parent, false);
                return new HeaderHolder(view);
            case 1:
                View vieww = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_who_is_who_filter_item, parent, false);
                return new Holder(vieww);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position){
            case 0:
                return 0;
            default:
                return 1;

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case 0:
                if(category){
                    ((HeaderHolder)holder).back.setVisibility(View.GONE);
                    ((HeaderHolder)holder).text.setText("ALL STAKEHOLDERS");
                }else{
                    ((HeaderHolder)holder).back.setVisibility(View.VISIBLE);
                    ((HeaderHolder)holder).text.setText("Back");
                }
                break;
            case 1:
                ((Holder)holder).partner.setText(data.get(position-1));

        }
    }

    @Override
    public int getItemCount() {
        return data.size()+1;
    }

    public class HeaderHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.imageView11)
        View back;
        @Bind(R.id.partner_text)
        TextView text;

        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(back.getVisibility()== View.VISIBLE){
                        data= people.getAllCategories();
                        category=true;
                        sub_category=false;
                        name=false;
                        notifyDataSetChanged();
                    }
                    else {
                        fragment.getPersonAdapter().setData(people.getPeople());
                        fragment.setCategory_selected("ALL STAKEHOLDERS");
                        fragment.getPersonAdapter().notifyDataSetChanged();
                        fragment.hideFilter();
                    }
                }
            });
        }
    }

    public class Holder extends RecyclerView.ViewHolder {

        @Bind(R.id.partner_text)
        TextView partner;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            partner.setTypeface(Aplication.hlight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    last_category = data.get(getAdapterPosition() - 1);
                    fragment.setCategory_selected(last_category);
                    //data = people.getSubCategory(last_category);
                    //category=false;
                    sub_category = true;
                    name = false;
                    if(last_category.toLowerCase().contains("all sta"))
                        fragment.getPersonAdapter().setData(people.getPeople());
                        else
                    fragment.getPersonAdapter().setData(people.getpersonForCategory(last_category));
                    fragment.getPersonAdapter().notifyDataSetChanged();
                    fragment.hideFilter();


//                    else if(sub_category){
//                        if(getAdapterPosition()==1){
//                            fragment.getPersonAdapter().setData(people.getpersonForCategory(last_category));
//                            fragment.getPersonAdapter().notifyDataSetChanged();
//                            fragment.hideFilter();
//                        }else {
//                            fragment.getPersonAdapter().setData(people.getPersonForSubCategories(data.get(getAdapterPosition()-1)));
//                            fragment.setCategory_selected(data.get(getAdapterPosition()-1));
//                        fragment.getPersonAdapter().notifyDataSetChanged();
//                        fragment.hideFilter();
//                        }
//                    }
                }
            });
        }
    }


    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }

    public boolean isCategory() {
        return category;
    }

    public void setCategory(boolean category) {
        this.category = category;
    }

    public boolean isSub_category() {
        return sub_category;
    }

    public void setSub_category(boolean sub_category) {
        this.sub_category = sub_category;
    }

    public boolean isName() {
        return name;
    }

    public void setName(boolean name) {
        this.name = name;
    }
}
