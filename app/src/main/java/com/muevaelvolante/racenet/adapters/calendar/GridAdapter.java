package com.muevaelvolante.racenet.adapters.calendar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.model.race_agenda.DayEvent;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GridAdapter extends ArrayAdapter {

    private static final String TAG = GridAdapter.class.getSimpleName();
    private final Context context;
    private LayoutInflater mInflater;
    private List<DayEvent> monthlyDates;
    private Calendar currentDate;
    private int screenWidth=-1;

    public GridAdapter(Context context, List<DayEvent> monthlyDates, Calendar currentDate) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        mInflater = LayoutInflater.from(context);
        this.context= context;
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Date mDate = monthlyDates.get(position).getDate();
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        View view = convertView;
        if(view == null){
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }
        View container= view.findViewById(R.id.container);
        View divider=view.findViewById(R.id.divider);
        //Add day to calendar
        TextView cellNumber = (TextView)view.findViewById(R.id.calendar_date_id);
        cellNumber.setTypeface(Aplication.hregular);
        cellNumber.setText(String.valueOf(dayValue));
        if(displayMonth == currentMonth && displayYear == currentYear){
            container.setVisibility(View.VISIBLE);
            divider.setVisibility(View.VISIBLE);
        }else{
            container.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
        }
        if(monthlyDates.get(position).isEvent()==2){
            cellNumber.setBackgroundResource(R.drawable.circle_blue);
        }else if (monthlyDates.get(position).isEvent()==1)
            cellNumber.setBackgroundResource(R.drawable.circle_grey);
        else
            cellNumber.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        ViewGroup.LayoutParams lp= view.getLayoutParams();
        lp.height= Aplication.screenW/7;
        lp.width= Aplication.screenW/7;
        view.setLayoutParams(lp);
        view.setTag(monthlyDates.get(position));
        return view;
    }
    @Override
    public int getCount() {
        return monthlyDates.size();
    }
    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }
    @Override
    public int getPosition(Object item) {
        return monthlyDates.indexOf(item);
    }


}