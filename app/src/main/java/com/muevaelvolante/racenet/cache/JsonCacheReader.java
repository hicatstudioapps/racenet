package com.muevaelvolante.racenet.cache;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 28/04/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class JsonCacheReader {

    public static String getCacheJson(String path) {
        try {
        BufferedReader reader= new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        StringBuilder json= new StringBuilder();
            String line= reader.readLine();
            while (line!= null){
                json.append(line);
                line=reader.readLine();
            }
            return json.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
