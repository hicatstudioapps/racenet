package com.muevaelvolante.racenet.cache;

import android.os.AsyncTask;
import android.speech.tts.Voice;
import android.util.Log;

import java.io.FileWriter;
import java.io.IOException;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 28/04/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class JsonCacheWriter  {

    public static void writeCache(String... params) {
        String src= params[0];
        String data= params[1];

        try {

            FileWriter writer= new FileWriter(src);
            writer.write(data);
            writer.close();
            Log.d("JsonCacheWriter", src+" OK");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
