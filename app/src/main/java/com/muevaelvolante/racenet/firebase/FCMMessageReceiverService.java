package com.muevaelvolante.racenet.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.activity.SplashActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

/**
 * Created by admin on 02/02/2017.
 */

public class FCMMessageReceiverService extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.w("fcm", "received notification");
///        String data= remoteMessage.getMessageId()+" "+remoteMessage.getData().toString()+ remoteMessage.getNotification().toString();
        Map<String,String> map=remoteMessage.getData();
         Set<String> key= map.keySet();
        String content= map.get("message");
        try {
            JSONObject object= new JSONObject(content);
            String title= object.getString("title");
            String message= object.getString("msg");
            String action= object.getString("action");
            String target= object.getString("target");
            String push_id="";
            if(object.has("push_message_id"))
            push_id=object.getString("push_message_id");
            Log.d("json", object.getString("msg"));
            Intent pending= new Intent(getApplicationContext(), SplashActivity.class);
            pending.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle data= new Bundle();
            data.putString("action",action);
            data.putString("target",target);
            data.putString("push_id",push_id);
            pending.putExtras(data);
            postNotification(title,message,pending,null);
            Intent sendToHome= new Intent("PUSH_ADDED");
            Bundle bundle= new Bundle();
            bundle.putString("message",content);
            sendToHome.putExtras(bundle);
            LocalBroadcastManager.getInstance(this).sendBroadcast(sendToHome);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void postNotification(String title, String msg, Intent intentAction, FCMMessageReceiverService notifyIntentService) {
        final NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentAction, Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL|PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] pattern = new long[] { 500, 500, 1000, 1000, 500, 500 };
        final Notification notification = new android.support.v4.app.NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setSound(sound)
                .setVibrate(pattern)
                .setVisibility(android.support.v4.app.NotificationCompat.VISIBILITY_PUBLIC)
                .getNotification();
        mNotificationManager.notify(23, notification);
        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
            wl_cpu.acquire(3000);
        }
    }
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageBody)
                .setAutoCancel(false)
                .setSound(defaultSoundUri);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notificationBuilder.build());
    }
}
