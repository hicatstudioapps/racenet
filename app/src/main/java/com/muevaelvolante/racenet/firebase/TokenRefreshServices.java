package com.muevaelvolante.racenet.firebase;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.BuildConfig;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.manager.ManagerLanguages;
import com.muevaelvolante.racenet.model.token.TokenData;
import com.muevaelvolante.racenet.service.ServiceGenerator;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 02/02/2017.
 */

public class TokenRefreshServices extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        String storedToken= getSharedPreferences(Constants.SETTINGS,MODE_PRIVATE).getString(Constants.TOKEN,"");
        getSharedPreferences(Constants.SETTINGS,MODE_PRIVATE).edit().putString(Constants.TOKEN,refreshedToken).commit();
        sendRegistrationToServer(refreshedToken);
        Log.d("notification", refreshedToken);
    }

    public static void sendRegistrationToServer(String refreshedToken) {
        String url="";
        TokenData tokenData= new TokenData();
        tokenData.setAppIdentifier("com.muevaelvolante.racenet");
        tokenData.setAppVersion(BuildConfig.VERSION_CODE+"");
        tokenData.setDeviceLang(ManagerLanguages.getLang());
        tokenData.setDeviceToken(refreshedToken);
        tokenData.setDeviceModel(Build.MODEL);
        tokenData.setDevicePlatform("android");
        if(com.muevaelvolante.racenet.Constants.dev){
            url=" https://3sxzfrj9ti.execute-api.us-east-1.amazonaws.com/dev/mobile/devices";
        }else
            url="https://o6u8zm036e.execute-api.us-east-1.amazonaws.com/prod/mobile/devices";
        String finalUrl = url;
        Observable.create(subscriber -> {
            ISettings service= ServiceGenerator.createService(ISettings.class);
            String json= new Gson().toJson(tokenData,TokenData.class);
            Call<ResponseBody> call= service.sendToken(finalUrl,tokenData);
            try {
             Response<ResponseBody> body=   call.execute();
                if(body.isSuccessful())
                    Log.d("DEVICE INFO SEND", "OK");
                else
                    Log.d("DEVICE INFO SEND", "FAIL");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).subscribeOn(Schedulers.io())
        .subscribe((o)-> {});
    }

    public static void sendOpenPush(String push_id, Context context) {//se abrio el push
        String url="";
        TokenData tokenData= new TokenData();
        tokenData.setAppIdentifier("com.muevaelvolante.racenet");
        tokenData.setAppVersion(BuildConfig.VERSION_CODE+"");
        tokenData.setDeviceLang(ManagerLanguages.getLang());
        tokenData.setDeviceToken(context.getSharedPreferences(Constants.SETTINGS,MODE_PRIVATE).getString(Constants.TOKEN,""));
        tokenData.setPush_message_id(push_id);
        tokenData.setDeviceModel(Build.MODEL);
        tokenData.setDevicePlatform("android");
        if(com.muevaelvolante.racenet.Constants.dev){
            url=" https://3sxzfrj9ti.execute-api.us-east-1.amazonaws.com/dev/mobile/devices";
        }else
            url="https://o6u8zm036e.execute-api.us-east-1.amazonaws.com/prod/mobile/devices";
        String finalUrl = url;
        Observable.create(subscriber -> {
            ISettings service= ServiceGenerator.createService(ISettings.class);
            String json= new Gson().toJson(tokenData,TokenData.class);
            Call<ResponseBody> call= service.sendToken(finalUrl,tokenData);
            try {
                Response<ResponseBody> body=   call.execute();
                if(body.isSuccessful())
                    Log.d("DEVICE INFO SEND", "OK");
                else
                    Log.d("DEVICE INFO SEND", "FAIL");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).subscribeOn(Schedulers.io())
                .subscribe((o)-> {});
    }
}
