package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.EventMapActivity;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.adapters.SmartFragmentStatePagerAdapter;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.model.agenda.Agenda;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgendaFragment extends RootFragment {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    private String mParam2;
    private ViewPagerAdapter adapter;
    private Menu menu;

    public AgendaFragment() {
        // Required empty public constructor
    }
    String selected_tabColor = "#ffffff";
    String unSelected_tabColor = "#64c5c5c8";

    @Bind(R.id.imageView14)
    ImageView image;
    @Bind(R.id.textView15)
    TextView short_text;
    @Bind(R.id.textView16)
    TextView text;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.date)
    TextView date;
    @Bind(R.id.text_container)
    LinearLayout text_container;
    @Bind(R.id.imageView15)
    View shadow;
    @Bind(R.id.viewpager)
    ViewPager pager;
    @Bind(R.id.btn_map)
    View event_map;
    @Bind(R.id.imageView5)
    View sch_indicator;
    @Bind(R.id.textView10)
    TextView sch_text;
    @Bind(R.id.imageView6)
    View spk_indicator;
    @Bind(R.id.textView9)
    TextView spk_text;
    @Bind(R.id.map)
    TextView map;
    @Bind(R.id.imageView7)
    ImageView mapIMG;
    @Bind(R.id.tab_container)
    View tab_view;

    Agenda agenda;

    @Override
    public void proccessData(Object data) {
        agenda = new Gson().fromJson((String) data, Agenda.class);
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(ScheduleFragment.newInstance(agenda), "ONE");
        if(agenda.getVenueMap()== null)
        adapter.addFragment(VenueFragment.newInstance("",""),"Trhee");
        else
            adapter.addFragment(VenueFragment.newInstance(agenda.getVenueMap().get(0).getMedia().get(0).getFile(),""),"Trhee");
        adapter.addFragment(VenueMapFragment.newInstance(agenda.getCityMap()),"");
        adapter.addFragment(new EmptyFragment(), "THREE");
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                   // menu.clear();
                    menu.getItem(0).setVisible(false);
                    spk_indicator.setVisibility(View.INVISIBLE);
                    spk_text.setTextColor(Color.parseColor(unSelected_tabColor));
                    sch_indicator.setVisibility(View.VISIBLE);
                    sch_text.setTextColor(Color.parseColor(selected_tabColor));
                    mapIMG.setVisibility(View.INVISIBLE);
                    map.setTextColor(Color.parseColor(unSelected_tabColor));
                    return;
                }else if(position==1){
                    //menu.clear();
                    new Handler().postDelayed(() -> menu.getItem(0).setVisible(true),500);
                    spk_indicator.setVisibility(View.VISIBLE);
                    spk_text.setTextColor(Color.parseColor(selected_tabColor));
                    sch_indicator.setVisibility(View.INVISIBLE);
                    sch_text.setTextColor(Color.parseColor(unSelected_tabColor));
                    mapIMG.setVisibility(View.INVISIBLE);
                    map.setTextColor(Color.parseColor(unSelected_tabColor));
                    return;
                }
                else if(position==2){
                    menu.getItem(0).setVisible(false);
                    spk_indicator.setVisibility(View.INVISIBLE);
                    spk_text.setTextColor(Color.parseColor(unSelected_tabColor));
                    sch_indicator.setVisibility(View.INVISIBLE);
                    sch_text.setTextColor(Color.parseColor(unSelected_tabColor));
                    mapIMG.setVisibility(View.VISIBLE);
                    map.setTextColor(Color.parseColor(selected_tabColor));
                    return;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        hideDialog();
    }

    @OnClick({R.id.btn_schedule, R.id.btn_speaker,R.id.btn_map})
    public void showTabs(View view){
        Animation anim= AnimationUtils.loadAnimation(getActivity(),R.anim.slide_in_forward);
        if(view.getId()==R.id.btn_speaker)
        pager.setCurrentItem(1);
        if(view.getId()==R.id.btn_map)
            pager.setCurrentItem(2);
        tab_view.setAnimation(anim);
        tab_view.setVisibility(View.VISIBLE);
    }

    public static AgendaFragment newInstance(String uri, String param2) {
        AgendaFragment fragment = new AgendaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, uri);
        args.putString("t", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name = "agenda";
            title_name = getArguments().getString("t");
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_empty,menu);
        this.menu= menu;
        this.menu.getItem(0).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).hideSearch();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.down) {
            ((PdfDonwLoadAction)adapter.getRegisteredFragment(pager.getCurrentItem())).savePdf();
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_agenda, container, false);
        ButterKnife.bind(this, layout);
        Bitmap b = BitmapFactory.decodeResource(Aplication.context.getResources(), R.drawable.generic_image_rectangular);
        float proportion = (float) b.getWidth() / b.getHeight();
        int alto = (int) ((Aplication.screenW) / proportion);
        ViewGroup.LayoutParams lp = (ViewGroup.LayoutParams) image.getLayoutParams();
        lp.height = alto;
        lp.width = Aplication.screenW;
        image.setLayoutParams(lp);
        shadow.setLayoutParams(lp);
        date.setTypeface(Aplication.hregular);
        title.setTypeface(Aplication.hlight);
        short_text.setTypeface(Aplication.hmedium);
        // ((FrameLayout)image.getParent()).setLayoutParams(lp);
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        return layout;
    }

    @OnClick(R.id.root_indicator2)
    public void speakerClick(){
        pager.setCurrentItem(1);
    }

    @OnClick(R.id.root_indicator1)
    public void scheduleClick(){
        pager.setCurrentItem(0);
    }
    @OnClick(R.id.root_indicator3)
    public void mappClick(){
        pager.setCurrentItem(2);
    }

    class ViewPagerAdapter extends SmartFragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
          //  manager.getFragments().clear();
            pager.setOffscreenPageLimit(0);
        }

        @Override
        public Fragment getItem(int position) {
               return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public interface PdfDonwLoadAction{
     void savePdf();
    }
}
