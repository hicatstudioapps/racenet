package com.muevaelvolante.racenet.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.adapters.MenuAdapter;
import com.muevaelvolante.racenet.model.setting.Menu;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FragmentMenu extends Fragment {

    private MenuListener mListener;

    public void setMenuItems(ArrayList<Menu> menuItems) {
        this.menuItems = menuItems;
        menuAdapter.setMenuItems(this.menuItems);
        menuAdapter.notifyDataSetChanged();
    }

    ArrayList<Menu> menuItems;
    MenuAdapter menuAdapter;
    @Bind(R.id.list)
    RecyclerView recyclerView;
    @Bind(R.id.textView7)
    TextView app;
    @Bind(R.id.imageView2)
    ImageView back;
    @Bind(R.id.root)
    View root;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FragmentMenu() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static FragmentMenu newInstance(int columnCount,ArrayList<Menu> menu) {
        FragmentMenu fragment = new FragmentMenu();
        Bundle args = new Bundle();
        args.putSerializable("m",menu);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            menuItems= (ArrayList<Menu>) getArguments().getSerializable("m");
            List<Menu> data= Stream.of(menuItems).filter(value -> !value.getID().toLowerCase().contains("notif")).collect(Collectors.toList());
            menuItems.clear();
            menuItems.addAll(data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_fragment, container, false);
        ButterKnife.bind(this,view);
        app.setTypeface(Aplication.hthin);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"asdasd",Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAdapter=new MenuAdapter(Aplication.context,menuItems,mListener);
        recyclerView.setAdapter(menuAdapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.closeMenu();
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener= (MenuListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //a ver si funciona que vaya para el elemento del menu que quiero
    public void performAction(int position){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               View view= recyclerView.findViewHolderForAdapterPosition(2).itemView;
                view.performClick();
            }
        },3000);

    }

    public interface MenuListener {
        // TODO: Update argument type and name
        void closeMenu();
        void gotoSection(Fragment section);
        void logOut();
    }
}
