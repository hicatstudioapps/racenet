package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.model.race_schhedule.RSchedule;
import com.muevaelvolante.racenet.service.ServiceGenerator;
import com.rey.material.widget.ProgressView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RaceScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RaceScheduleFragment extends RootFragment implements OnLoadCompleteListener, AgendaFragment.PdfDonwLoadAction {

    private static final String ARG_PARAM1 = "param1";
    @Bind(R.id.loading)
    ProgressView progressView;
    @Bind(R.id.progress_text)
    TextView progress_text;
    @Bind(R.id.pdfView)
    PDFView pdfView;
    DonwLoadPdf donwLoadPdf;
    private boolean fileReady;

    public RaceScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public void proccessData(Object data) {
        String view= (String)data;
        RSchedule rece_schedule= new Gson().fromJson((String)data,RSchedule.class);
        donwLoadPdf=  new DonwLoadPdf();
        donwLoadPdf.execute(new String[]{rece_schedule.getRaceSchedule().get(0).getMedia().get(0).getFile()});
        hideDialog();
    }

    public static RaceScheduleFragment newInstance(String param1, String param2) {
        RaceScheduleFragment fragment = new RaceScheduleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString("t",param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name="race_schedule";
            title_name=getArguments().getString("t");
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
       super.onActivityCreated(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.down) {
            if (donwLoadPdf.getStatus() == AsyncTask.Status.RUNNING) {
                Toast.makeText(getActivity(), "File not ready yet.", Toast.LENGTH_SHORT).show();
                return true;
            }
            if (fileReady) {
                File dir = new File(Constants.appRootDir + File.separator + "PDFs");
                if (!dir.exists())
                    dir.mkdir();
                File outFile = new File(Constants.appRootDir + File.separator, "temp.pdf");
                if (outFile.exists()) {
                    boolean result = outFile.renameTo(new File(Constants.appRootDir + File.separator + "PDFs" + File.separator + System.currentTimeMillis() + ".pdf"));
                    if (result)
                        Toast.makeText(getActivity(), "File saved in " + outFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Error saving file", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_empty,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).setToolBarTitle(title_name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout= inflater.inflate(R.layout.fragment_race_schedule, container, false);
        ButterKnife.bind(this,layout);
        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).hideSearch();
    }

    @Override
    public void savePdf() {
        savePdf();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((HomeActivity)getActivity()).setPdfInstance(null);
    }

    class DonwLoadPdf extends AsyncTask<String, Integer, Uri> {

        @Override
        protected Uri doInBackground(String... params) {
            ISettings settings = ServiceGenerator.createService(ISettings.class);
            Call<ResponseBody> fileStream = settings.downloadFilePdf(params[0]);
            try {
                ResponseBody body = fileStream.execute().body();
                File outFile = new File(Constants.appRootDir+File.separator, "temp.pdf");
                int fileLength = (int) body.contentLength();
                InputStream input = new BufferedInputStream(body.byteStream());
                OutputStream output = null;
                output = new FileOutputStream(outFile);
                byte data[] = new byte[1024];
                long total = 0;
                int count = 0;
                while ((count = (input.read(data))) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    publishProgress(Integer.valueOf("" + (int) (total * 100 / fileLength)));
                }
                output.flush();
                output.close();
                input.close();
                return Uri.parse(outFile.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.start();
        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            if (uri == null) {
                progress_text.setText("Error loading file..");
                progressView.stop();
            } else {
                progress_text.setText("Opening file...");
                pdfView.fromUri(uri)
                        .enableAnnotationRendering(true)
                        .onLoad(RaceScheduleFragment.this)
                        .swipeHorizontal(true)
                        .load();
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progress_text.setText(values[0] + "%");
        }
    }

    @Override
    public void loadComplete(int nbPages) {
        progressView.stop();
        progress_text.setVisibility(View.GONE);
        fileReady=true;
        if(getActivity()!=null) {
            Aplication application = (Aplication) getActivity().getApplication();
            Tracker mTracker = application.getDefaultTracker();
            mTracker.setScreenName("RaceSchedule");
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("RaceSchedule")
                    .setAction("Donwload")
                    .setLabel("RaceSchedule donwloaded")
                    .setValue(1)
                    .build());
        }
    }
}
