package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.service.ServiceGenerator;
import com.rey.material.widget.ProgressView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VenueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VenueFragment extends Fragment implements OnLoadCompleteListener, AgendaFragment.PdfDonwLoadAction {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.loading)
    ProgressView progressView;
    @Bind(R.id.progress_text)
    TextView progress_text;
    @Bind(R.id.pdfView)
    PDFView pdfView;

    // TODO: Rename and change types of parameters
    private String url;
    private String mParam2;
    private DonwLoadPdf donwLoadPdf;
    private boolean fileReady;
    private ImageView imgDonload=null;

    public VenueFragment() {
        // Required empty public constructor
    }

    public static VenueFragment newInstance(String url, String param2) {
        VenueFragment fragment = new VenueFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            url = getArguments().getString("url");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_venue, container, false);
        ButterKnife.bind(this, view);
        if(TextUtils.isEmpty(url)){
            progress_text.setText("No Data");
            progress_text.setVisibility(View.VISIBLE);
        }else {
            donwLoadPdf = new DonwLoadPdf();
            donwLoadPdf.execute(new String[]{url});
        }
        if (imgDonload!= null)
            imgDonload.setOnClickListener(view1 -> savePdf());
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).setPdfInstance(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.down) {
          savePdf();
        }
        return true;
    }

    public void savePdf() {

            if (donwLoadPdf.getStatus() == AsyncTask.Status.RUNNING) {
                Toast.makeText(getActivity(), "File not ready yet.", Toast.LENGTH_SHORT).show();
            }
            if (fileReady) {
                File dir = new File(Constants.appRootDir + File.separator + "PDFs");
                if (!dir.exists())
                    dir.mkdir();
                File outFile = new File(Constants.appRootDir + File.separator, "temp.pdf");
                if (outFile.exists()) {
                    boolean result = outFile.renameTo(new File(Constants.appRootDir + File.separator + "PDFs" + File.separator + System.currentTimeMillis() + ".pdf"));
                    if (result)
                        Toast.makeText(getActivity(), "File saved in " + outFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Error saving file", Toast.LENGTH_SHORT).show();
            }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_empty,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void loadComplete(int nbPages) {
        progressView.stop();
        progress_text.setVisibility(View.GONE);
        fileReady=true;
    }

    public void setImgDonload(ImageView imgDonload) {
        this.imgDonload = imgDonload;
    }

    class DonwLoadPdf extends AsyncTask<String, Integer, Uri> {

        @Override
        protected Uri doInBackground(String... params) {
            ISettings settings = ServiceGenerator.createService(ISettings.class);
            Call<ResponseBody> fileStream = settings.downloadFilePdf(params[0]);
            try {
                ResponseBody body = fileStream.execute().body();
                File outFile = new File(Constants.appRootDir, "temp.pdf");
                int fileLength = (int) body.contentLength();
                InputStream input = new BufferedInputStream(body.byteStream());
                OutputStream output = null;
                output = new FileOutputStream(outFile);
                byte data[] = new byte[1024];
                long total = 0;
                int count = 0;
                while ((count = (input.read(data))) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    publishProgress(Integer.valueOf("" + (int) (total * 100 / fileLength)));
                }
                output.flush();
                output.close();
                input.close();
                return Uri.parse(outFile.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            if (uri == null) {
                progress_text.setText("Error loading file..");
                progressView.stop();
            } else {
                progress_text.setText("Opening file...");
                pdfView.fromUri(uri)
                        .enableAnnotationRendering(true)
                        .onLoad(VenueFragment.this)
                        .load();
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progress_text.setText(values[0] + "%");
        }
    }
}
