package com.muevaelvolante.racenet.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.adapters.WhoIsWhoAdapter;
import com.muevaelvolante.racenet.adapters.WhoIsWhoFilterAdapter;
import com.muevaelvolante.racenet.model.who.Category;
import com.muevaelvolante.racenet.model.who.People;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

@SuppressWarnings("WrongConstant")
public class WhoIsWhoFragment extends RootFragment implements HomeActivity.ToolBarTitleAction{

    private static final String ARG_PARAM1 = "param1";
    People people;
    @Bind(R.id.list)
    RecyclerView recyclerView;
    @Bind(R.id.list2)
    RecyclerView filterRecyclerView;
    @Bind(R.id.filter_container)
    View filter_container;
    @Bind(R.id.editText)
    EditText filter_textl;
    @Bind(R.id.category_selected)
    TextView category_selected;

    private WhoIsWhoAdapter personAdapter;
    private boolean isFilterOpen=false;

    public WhoIsWhoAdapter getPersonAdapter() {
        return personAdapter;
    }

    private HomeActivity homeActivity;
    private ArrayList<String> categoryFilter;
    WhoIsWhoFilterAdapter filterAdapter;

    public WhoIsWhoFragment() {
        // Required empty public constructor
    }

    public void setCategory_selected(String cat){
        category_selected.setText(cat);
    }
    @Override
    public void proccessData(Object data) {
        people= new Gson().fromJson((String) data,People.class);
        category_selected.setText("ALL STAKEHOLDERS ");
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        personAdapter= new WhoIsWhoAdapter(people.getPeople(),getContext());
        recyclerView.setAdapter(personAdapter);
        //otenemos los primeros filtros
        categoryFilter= people.getAllCategories();
        filterAdapter= new WhoIsWhoFilterAdapter(people,this,categoryFilter,true,false,false);
        filterRecyclerView.setAdapter(filterAdapter);
        filterRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).color(Color.parseColor("#ffffff")).build());
        hideDialog();
    }

    public static WhoIsWhoFragment newInstance(String uri, String param2) {
        WhoIsWhoFragment fragment = new WhoIsWhoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, uri);
        args.putString("t",param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name="who";
            title_name=getArguments().getString("t");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout= inflater.inflate(R.layout.fragment_who_is_who, container, false);
        ButterKnife.bind(this,layout);
        category_selected.setTypeface(Aplication.hregular);
        category_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        filter_textl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    personAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        filter_textl.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    hideFilter();
                    InputMethodManager obj = (InputMethodManager)getContext().getSystemService("input_method");
                    if (obj != null)
                    {
                        ((InputMethodManager) (obj)).hideSoftInputFromWindow(filter_textl.getWindowToken(), 0);
                    }
}
                return false;
            }
        });
        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        homeActivity= (HomeActivity) context;
        homeActivity.spinnerOn();
        homeActivity.showSearch();
    }

    @Override
    public void toolBarClick() {
        if(!isFilterOpen) {
            isFilterOpen=true;
            filter_container.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slide_top_in));
            filter_container.setVisibility(View.VISIBLE);
        }else
        {
            filter_container.setAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.slide_top_out));
            filter_container.setVisibility(View.GONE);
            isFilterOpen=false;
        }
    }

    public void hideFilter(){
        filter_container.setAnimation(AnimationUtils.loadAnimation(getContext(),R.anim.slide_top_out));
        filter_container.setVisibility(View.GONE);
    }
}
