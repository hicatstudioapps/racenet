package com.muevaelvolante.racenet.fragment.race_agenda;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.CalendarCustomView;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.fragment.RootFragment;
import com.muevaelvolante.racenet.model.race_agenda.Agenda;
import com.muevaelvolante.racenet.model.race_agenda.Filter_;
import com.muevaelvolante.racenet.model.race_agenda.RaceAgenda;

import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RaceAgendaCalendarFragment extends RootFragment {
    private static final String ARG_PARAM1 = "param1";

    @Bind(R.id.calendarr)
    CalendarCustomView calendar;

    RaceAgendaFragment raceAgendaFragment;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.back)
    ImageView back;
    @Bind(R.id.imageView23)
    ImageView imageView23;
    private String fileUrl;
    private List<Filter_> filters;
    private RaceScheduleFragmentNew listener;

    public List<Filter_> getFilters() {
        return filters;
    }

    public RaceAgendaCalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void proccessData(Object data) {
        RaceAgenda raceAgenda = new Gson().fromJson((String) data, RaceAgenda.class);
        // buscamos la primera fecha de los eventos para saber el mes
        String date = Stream.of(raceAgenda.getAgenda()).filter(value -> !TextUtils.isEmpty(value.getDate())).findFirst().get().getDate();
        calendar.setAgenda(raceAgenda.getAgenda());
        calendar.setListener(this);
        calendar.init(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withLocale(Locale.ENGLISH).parseDateTime(date).toCalendar(Locale.ENGLISH));
        fileUrl = raceAgenda.getVenueMap().get(0).getMedia().get(0).getFile();
        filters = raceAgenda.getFilters();
    }

    // TODO: Rename and change types and number of parameters
    public static RaceAgendaCalendarFragment newInstance(String uri, String param2) {
        RaceAgendaCalendarFragment fragment = new RaceAgendaCalendarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, uri);
        args.putString("t", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name = title_name;
            title_name = getArguments().getString("t");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_race_agenda_calendar, container, false);
        ButterKnife.bind(this, view);
        view.findViewById(R.id.root).setOnClickListener(view1 -> {
        });
        title.setTypeface(Aplication.hmedium);
        title.setText(((HomeActivity) getActivity()).getToolBarTitle());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void goToRaceAgenda(LinkedTreeMap<Integer, ArrayList<Agenda>> treeMap, int position) {
        raceAgendaFragment = RaceAgendaFragment.newInstance(treeMap, title_name + " Schedule", fileUrl);
        raceAgendaFragment.setListener(this);
        raceAgendaFragment.setPosition(position);
        raceAgendaFragment.setImgDonload(imageView23);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward).replace(R.id.agenda_container, raceAgendaFragment)
                .commit();
//        ((HomeActivity)getActivity()).hideNavigationIcon();
    }

    public void initRaceAgenda(LinkedTreeMap<Integer, ArrayList<Agenda>> treeMap) {
        raceAgendaFragment = RaceAgendaFragment.newInstance(treeMap, title_name + " Schedule", fileUrl);
        raceAgendaFragment.setListener(this);
        raceAgendaFragment.setImgDonload(imageView23);
    }

    public void closeAgenda() {
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_forward, R.anim.slide_out_forward).remove(raceAgendaFragment)
                .commit();
        ((HomeActivity) getActivity()).showNavigationIcon();

    }

    @OnClick(R.id.back)
    public void onViewClicked() {
        listener.back();
    }

    public void setListener(RaceScheduleFragmentNew listener) {
        this.listener = listener;
    }
}
