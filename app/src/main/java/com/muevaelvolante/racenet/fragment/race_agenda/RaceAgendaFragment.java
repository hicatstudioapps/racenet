package com.muevaelvolante.racenet.fragment.race_agenda;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.adapters.RaceAgendaFilterAdapter;
import com.muevaelvolante.racenet.adapters.SmartFragmentStatePagerAdapter;
import com.muevaelvolante.racenet.fragment.AgendaFragment;
import com.muevaelvolante.racenet.fragment.VenueFragment;
import com.muevaelvolante.racenet.model.race_agenda.Agenda;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RaceAgendaFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.textView22)
    TextView textView22;
    @Bind(R.id.textView23)
    TextView textView23;
    @Bind(R.id.calendar_text)
    TextView calendarText;
    @Bind(R.id.tv_filter)
    TextView tvFilter;
    @Bind(R.id.filter_value)
    TextView filterValue;
    @Bind(R.id.top)
    LinearLayout top;
    @Bind(R.id.root_indicator1)
    LinearLayout rootIndicator1;
    @Bind(R.id.root_indicator2)
    LinearLayout rootIndicator2;
    @Bind(R.id.tab)
    LinearLayout tab;
    @Bind(R.id.tab_container)
    FrameLayout tabContainer;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    LinkedTreeMap<Integer, ArrayList<Agenda>> treeMap;
    @Bind(R.id.imageView21)
    ImageView imageView21;
    @Bind(R.id.imageView20)
    ImageView imageView20;
    @Bind(R.id.img_calendar)
    ImageView imgCalendar;
    @Bind(R.id.img_filter)
    ImageView imgFilter;
    @Bind(R.id.filter_root)
    RelativeLayout filterRoot;
    String title;
    @Bind(R.id.spk_indicator)
    ImageView spk_indicator;
    @Bind(R.id.spk_text)
    TextView spk_text;
    @Bind(R.id.sch_indicator)
    ImageView sch_indicator;
    @Bind(R.id.sch_text)
    TextView sch_text;
    @Bind(R.id.filter_list)
    RecyclerView filterList;

    private ViewPagerAdapter adapter;
    private int position = 0;
    private RaceAgendaCalendarFragment listener;
    private Menu menu;

    String selected_tabColor = "#ffffff";
    String unSelected_tabColor = "#64c5c5c8";
    private String url;
    private int lastPosition = 0;// ultima posicion del view pager antes de ir al mapa
    private ImageView imgDonload;

    public RaceAgendaFragment() {
        // Required empty public constructor
    }

    public static RaceAgendaFragment newInstance(LinkedTreeMap<Integer, ArrayList<Agenda>> treeMap, String title,
                                                 String urlFile) {
        RaceAgendaFragment fragment = new RaceAgendaFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, treeMap);
        args.putString("t", title);
        args.putString("url", urlFile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treeMap = (LinkedTreeMap<Integer, ArrayList<Agenda>>) getArguments().getSerializable(ARG_PARAM1);
            title = getArguments().getString("t");
            url = getArguments().getString("url");
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_race_agenda, container, false);
        ButterKnife.bind(this, view);
        view.setOnClickListener(view1 -> {
        });
        textView23.setTypeface(Aplication.hlight);
        spk_text.setTypeface(Aplication.hregular);
        sch_text.setTypeface(Aplication.hregular);
        textView22.setTypeface(Aplication.hregular);
        calendarText.setTypeface(Aplication.hregular);
        filterValue.setTypeface(Aplication.hregular);
        tvFilter.setTypeface(Aplication.hregular);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        //cargamos filtros
        filterList.setLayoutManager(new LinearLayoutManager(getContext()));
        filterList.setAdapter(new RaceAgendaFilterAdapter(listener.getFilters(), this));
        viewpager.setOffscreenPageLimit(2);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                new Handler().postDelayed(() -> {// vamos directo al pdf
                    if (position == treeMap.size()){
                        imgDonload.setVisibility(View.VISIBLE);
                        speakerClick();
                    }
                    else {// cargamos datos de la fecha
                        imgDonload.setVisibility(View.INVISIBLE);
                        lastPosition = position;
                        textView23.setText(((RaceAgendaItem) adapter.getRegisteredFragment(position)).getDayString());
                        if (((RaceAgendaItem) adapter.getRegisteredFragment(position)).isEmpty)//si no hay eventos escondemos los filtros
                            filterRoot.setVisibility(View.GONE);
                        else
                            filterRoot.setVisibility(View.VISIBLE);
                        filterValue.setText("");
                        ((RaceAgendaItem) adapter.getRegisteredFragment(position)).rv.setVisibility(View.VISIBLE);
                        ((RaceAgendaItem) adapter.getRegisteredFragment(position)).empty.setVisibility(View.VISIBLE);
                    }
                }, 500);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        viewpager.setAdapter(adapter);
        viewpager.setCurrentItem(position);
        new Handler().postDelayed(() ->
                textView23.setText(((RaceAgendaItem) adapter.getRegisteredFragment(position)).getDayString()), 400);
//                if (position == treeMap.size() - 1) {
//                    imageView21.setVisibility(View.INVISIBLE);
//                    textView22.setVisibility(View.INVISIBLE);
//                }else {
//                    imageView21.setVisibility(View.INVISIBLE);
//                    textView22.setVisibility(View.INVISIBLE);
//                }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_empty,menu);
        this.menu= menu;
        this.menu.getItem(0).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.down) {
            ((AgendaFragment.PdfDonwLoadAction)adapter.getRegisteredFragment(viewpager.getCurrentItem())).savePdf();
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    @OnClick({R.id.calendar_text, R.id.img_calendar})
    public void onClick(View view) {
        listener.closeAgenda();
    }

    @OnClick({R.id.imageView21, R.id.textView22})
    public void clickNext() {
        if (viewpager.getCurrentItem() < treeMap.size() - 1)
            viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
    }

    @OnClick(R.id.imageView20)
    public void clickBack() {
        if (viewpager.getCurrentItem() > 0)
            viewpager.setCurrentItem(viewpager.getCurrentItem() - 1);
    }

    @OnClick(R.id.root_indicator2)
    public void speakerClick() {
        spk_indicator.setVisibility(View.INVISIBLE);
        spk_text.setTextColor(Color.parseColor(unSelected_tabColor));
        sch_indicator.setVisibility(View.VISIBLE);
        sch_text.setTextColor(Color.parseColor(selected_tabColor));
        viewpager.setCurrentItem(treeMap.size());
        top.setVisibility(View.GONE);
    }

    @OnClick(R.id.root_indicator1)
    public void scheduleClick() {
        spk_indicator.setVisibility(View.VISIBLE);
        spk_text.setTextColor(Color.parseColor(selected_tabColor));
        sch_indicator.setVisibility(View.INVISIBLE);
        sch_text.setTextColor(Color.parseColor(unSelected_tabColor));
        top.setVisibility(View.VISIBLE);
        viewpager.setCurrentItem(lastPosition);
    }

    @OnClick({R.id.tv_filter, R.id.img_filter})
    public void onClickFilter(View view) {
        showFilter();
    }

    public void showFilter() {
        if (filterList.getVisibility() == View.VISIBLE) {
            filterList.setAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out));
            filterList.setVisibility(View.GONE);
        } else {
            filterList.setAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in));
            filterList.setVisibility(View.VISIBLE);
        }
    }

    public void filter(String target){
        showFilter();
        if(target.toLowerCase().equals("all"))
            filterValue.setText("");
        else
            filterValue.setText(target);
        ((RaceAgendaItem)adapter.getRegisteredFragment(viewpager.getCurrentItem())).filter(target);
    }
    public void setListener(RaceAgendaCalendarFragment listener) {
        this.listener = listener;
    }

    public RaceAgendaCalendarFragment getListener() {
        return listener;
    }

    public void setImgDonload(ImageView imgDonload) {
        this.imgDonload = imgDonload;
    }

    public ImageView getImgDonload() {
        return imgDonload;
    }

    class ViewPagerAdapter extends SmartFragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == treeMap.size()) {
                VenueFragment venueMap= VenueFragment.newInstance(url, "");
                venueMap.setImgDonload(imgDonload);
                return venueMap;
            }
            else
                return RaceAgendaItem.newInstance(treeMap.get(position));
        }

        @Override
        public int getCount() {
            return treeMap.size() + 1;
        }
    }

}
