package com.muevaelvolante.racenet.fragment.race_agenda;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.Collector;
import com.annimon.stream.Stream;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.adapters.RaceAgendaAdapter;
import com.muevaelvolante.racenet.model.race_agenda.Agenda;
import com.muevaelvolante.racenet.model.race_agenda.Filter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RaceAgendaItem extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    ArrayList<Agenda> agendas;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.empty)
    TextView empty;
    boolean isEmpty=false;
    RaceAgendaAdapter adapter;

    public boolean isEmpty() {
        return isEmpty;
    }

    public RaceAgendaItem() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static RaceAgendaItem newInstance(ArrayList<Agenda> agendas) {
        RaceAgendaItem fragment = new RaceAgendaItem();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, agendas);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            agendas = (ArrayList<Agenda>) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_race_agenda_item, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    public void init(){
        empty.setTypeface(Aplication.hregular);
            rv.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter= new RaceAgendaAdapter(agendas,getContext());
            rv.setAdapter(adapter);
            empty.setVisibility(View.INVISIBLE);
    }

    public void filter(String target) {
        if (target.toLowerCase().equals("all")) {
            adapter.reset();
            adapter.notifyDataSetChanged();
            rv.setVisibility(View.VISIBLE);
            empty.setVisibility(View.INVISIBLE);
        } else {
            List<Agenda> results = new ArrayList<>();
            for (Agenda item :
                    agendas) {
                for (Filter filter :
                        item.getFilters()) {
                    if (filter.getFilter().contains(target.toLowerCase()))
                    {
                        results.add(item);
                        break;
                    }

                }
            }
            if (results.size() > 0) {
                adapter.setData(results);
                adapter.notifyDataSetChanged();
                rv.setVisibility(View.VISIBLE);
            }else {
                rv.setVisibility(View.INVISIBLE);
                empty.setVisibility(View.VISIBLE);
            }
        }
    }

    public void hideFilter(){

    }

    public String getDayString() {
        DateTime l = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withLocale(Locale.CANADA).parseDateTime(agendas.get(0).getDate());
        return l.dayOfWeek().getAsText(Locale.CANADA)+", "+l.monthOfYear().getAsText(Locale.CANADA)+" "+l.getDayOfMonth();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
