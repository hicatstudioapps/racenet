package com.muevaelvolante.racenet.fragment.race_agenda;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.R;
import com.muevaelvolante.racenet.activity.HomeActivity;
import com.muevaelvolante.racenet.adapters.RaceAgendaRoutesAdapter;
import com.muevaelvolante.racenet.fragment.RootFragment;
import com.muevaelvolante.racenet.model.race_agenda.list.ScheduleList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RaceScheduleFragmentNew extends RootFragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @Bind(R.id.textView20)
    TextView textView20;
    @Bind(R.id.rv)
    RecyclerView rv;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public RaceScheduleFragmentNew() {
        // Required empty public constructor
    }

    @Override
    public void proccessData(Object data) {
        ScheduleList scheduleList= new Gson().fromJson((String)data,ScheduleList.class);
        rv.setAdapter(new RaceAgendaRoutesAdapter(scheduleList.getAgenda(),this));
        hideDialog();
    }

    // TODO: Rename and change types and number of parameters
    public static RaceScheduleFragmentNew newInstance(String param1, String param2) {
        RaceScheduleFragmentNew fragment = new RaceScheduleFragmentNew();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString("t", param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            uri = getArguments().getString(ARG_PARAM1);
            json_name = "race-schedule";
            title_name = getArguments().getString("t");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_race_schedule2, container, false);
        ButterKnife.bind(this, view);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        textView20.setTypeface(Aplication.hmedium);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity)context).hideSearch();
        ((HomeActivity)context).spinnerOff();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).hideSearch();
        ((HomeActivity)getActivity()).spinnerOff();
    }

    public void goTo(Fragment fragment) {
        ((RaceAgendaCalendarFragment)fragment).setListener(this);
        ((RaceAgendaCalendarFragment)fragment).setUpdateTilte(false);
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_forward,R.anim.slide_out_forward).replace(R.id.container,fragment).addToBackStack("")
                .commit();
    }

    public void back(){
        getChildFragmentManager().popBackStack();
    }
}
