package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.web.ApiWeb;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 07/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public interface IData {
    @GET
    public Call<ResponseBody> getData(@Url String url);
}
