package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.web.ApiWeb;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 05/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public interface ILoginWeb {
    @GET
    public Call<ApiWeb> login(@Url String url,@QueryMap Map<String,String> mapData);
}
