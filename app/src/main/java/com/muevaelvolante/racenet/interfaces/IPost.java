package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.post.Post;
import com.muevaelvolante.racenet.model.search.Search;
import com.muevaelvolante.racenet.model.setting.Setting;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 05/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public interface IPost {
    @GET
    public Call<Post> getPosts(@Url String url);
    @GET
    public Call<Search> search(@Url String url);
}
