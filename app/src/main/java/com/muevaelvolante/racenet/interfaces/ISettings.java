package com.muevaelvolante.racenet.interfaces;

import com.muevaelvolante.racenet.model.setting.Setting;
import com.muevaelvolante.racenet.model.token.TokenData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 03/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public interface ISettings {
    @GET
    public Call<Setting> getData(@Url String url);

    @GET
    @Streaming
    Call<ResponseBody> downloadFilePdf(@Url String fileUrl);

    @POST
    public retrofit2.Call<ResponseBody> sendToken(@Url String url, @Body TokenData tokenData);
}
