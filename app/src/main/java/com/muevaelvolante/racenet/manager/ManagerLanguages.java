package com.muevaelvolante.racenet.manager;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;

import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.util.Utils;

import java.util.Locale;

/**
 * Created by admin on 26/01/2017.
 */

public class ManagerLanguages {

    /*
     * Constantes estáticas: idiomas (códigos ISO 639-1).
     *
     * La aplicación sólo funciona con inglés, español y francés, siendo este mismo el orden por defecto
     * en caso de problemas. Es decir, cualquier otro idioma no especificado debería devolver el idioma
     * inglés, que es el primero por defecto. Sólo hay una excepción, y ocurre cuando se trata del
     * idioma catalán, en el que devolveremos el español.
     */
    public final static String LANG_EN = "en";
    public final static String LANG_ES = "es";
    public final static String LANG_FR = "fr";
    public final static String LANG_CA = "ca";





    /**
     * Método para obtener el idioma con el que se trabajará con la API.
     *
     * @return
     */
    public static String getLang () {

        String codePhone = Utils.getLanguageCodeByPhone();

		/*
		 * Si no se puede obtener el idioma del dispositivo, devolvemos el idioma por defecto,
		 * el inglés.
		 */
        if ( codePhone == null ) {

            return LANG_EN;

        }

		/*
		 * Si es español ó catalán, devolvemos el idioma español.
		 */
        if ( codePhone.equals( LANG_ES ) || codePhone.equals( LANG_CA ) ) {

            return LANG_ES;

        }

		/*
		 * Si es fracés, devolvemos el idioma fracés.
		 */
        if ( codePhone.equals( LANG_FR ) ) {

            return LANG_FR;

        }

		/*
		 * En cualquier otro caso, devolvemos el idioma inglés.
		 */
        return LANG_EN;

    }

    public static String getAppLang(Context context){
        return context.getSharedPreferences(Constants.SETTINGS, Context.MODE_PRIVATE).getString(Constants.LANG, "");
    }

    public static void enableLanguage(AppCompatActivity context) {
        String currentInApp= context.getSharedPreferences(Constants.SETTINGS, Context.MODE_PRIVATE).getString(Constants.LANG, "");
        String currentInPhone= getLang();
        if(!currentInApp.equals(currentInPhone)){
            Locale locale = new Locale(currentInApp);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            context.getBaseContext().getResources().updateConfiguration(config,
                    context.getBaseContext().getResources().getDisplayMetrics());
        }

    }
}
