
package com.muevaelvolante.racenet.model.agenda;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import com.google.android.gms.fitness.request.DeleteAllUserDataRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Agenda implements Serializable{

    @SerializedName("home")
    @Expose
    private List<Home> home = new ArrayList<Home>();
    @SerializedName("day-1")
    @Expose
    private List<Day> day = new ArrayList<Day>();
    @SerializedName("day-2")
    @Expose
    private List<Day> day2 = new ArrayList<Day>();

    public List<Day> getDay3() {
        return day3;
    }

    public void setDay3(List<Day> day3) {
        this.day3 = day3;
    }

    @SerializedName("day-3")
    @Expose
    private List<Day> day3 = new ArrayList<Day>();
    @SerializedName("speakers")
    @Expose
    private ArrayList<Speaker> speakers = new ArrayList<Speaker>();
    @SerializedName("city map")
    @Expose
    private ArrayList<CityMap> cityMap = new ArrayList<CityMap>();
    @SerializedName("venue map")
    @Expose
    private List<VenueMap> venueMap = new ArrayList<VenueMap>();
    @SerializedName("filters")
    @Expose
    private List<Filter> filters = new ArrayList<Filter>();

    public List<Day> getDayFromFilter(String filter,int viewType){//view type es si es day1 o day2
        List<Day> result= new ArrayList<>();
        List<Day> query;
        if(viewType==0)
            query=day;
        else if (viewType==1){
            query=day2;
        }
        else
        query=day3;
        if(TextUtils.isEmpty(filter))//si no hay filtro devuelvo los datos de day1 o day2;
            return query;
        for (Day day :
                query) {
            for (Filter f :
                    day.getFilters()) {
                String data= f.getFilter();
                if(data.equals(filter))
                    result.add(day);
            }

        }
        return result;
    }
    /**
     * 
     * @return
     *     The home
     */
    public List<Home> getHome() {
        return home;
    }

    /**
     * 
     * @param home
     *     The home
     */
    public void setHome(List<Home> home) {
        this.home = home;
    }

    /**
     * 
     * @return
     *     The day
     */
    public List<Day> getDay1() {
        return day;
    }

    /**
     * 
     * @param day
     *     The day-1
     */
    public void setDay1(List<Day> day) {
        this.day = day;
    }

    /**
     * 
     * @return
     *     The day2
     */
    public List<Day> getDay2() {
        return day2;
    }

    /**
     * 
     * @param day2
     *     The day-2
     */
    public void setDay2(List<Day> day2) {
        this.day2 = day2;
    }

    /**
     * 
     * @return
     *     The speakers
     */
    public ArrayList<Speaker> getSpeakers() {
        return speakers;
    }

    /**
     * 
     * @param speakers
     *     The speakers
     */
    public void setSpeakers(ArrayList<Speaker> speakers) {
        this.speakers = speakers;
    }

    /**
     * 
     * @return
     *     The cityMap
     */
    public ArrayList<CityMap> getCityMap() {
        return cityMap;
    }

    /**
     * 
     * @param cityMap
     *     The city map
     */
    public void setCityMap(ArrayList<CityMap> cityMap) {
        this.cityMap = cityMap;
    }

    /**
     * 
     * @return
     *     The venueMap
     */
    public List<VenueMap> getVenueMap() {
        return venueMap;
    }

    /**
     * 
     * @param venueMap
     *     The venue map
     */
    public void setVenueMap(List<VenueMap> venueMap) {
        this.venueMap = venueMap;
    }

    /**
     * 
     * @return
     *     The filters
     */
    public List<Filter> getFilters() {
        return filters;
    }

    /**
     * 
     * @param filters
     *     The filters
     */
    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public Speaker findSpeakerById(String id){
        for (Speaker speaker :
                getSpeakers()) {
            if (speaker.getId().equals(id))
                return speaker;
        }
        return null;
    }

}
