
package com.muevaelvolante.racenet.model.agenda;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("org.jsonschema2pojo")
public class Filter implements Serializable{

    @SerializedName("filter")
    @Expose
    private String filter;

    /**
     * 
     * @return
     *     The filter
     */
    public String getFilter() {
        return filter;
    }

    /**
     * 
     * @param filter
     *     The filter
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String toString() {
        return filter;
    }
}
