
package com.muevaelvolante.racenet.model.notification;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class PushNotification {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ARN")
    @Expose
    private String aRN;
    @SerializedName("default")
    @Expose
    private String _default;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The aRN
     */
    public String getARN() {
        return aRN;
    }

    /**
     * 
     * @param aRN
     *     The ARN
     */
    public void setARN(String aRN) {
        this.aRN = aRN;
    }

    /**
     * 
     * @return
     *     The _default
     */
    public String getDefault() {
        return _default;
    }

    /**
     * 
     * @param _default
     *     The default
     */
    public void setDefault(String _default) {
        this._default = _default;
    }

}
