package com.muevaelvolante.racenet.model.partner;

import java.io.Serializable;
import java.util.List;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 09/08/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 */
public class Partner implements Serializable{


    String url;
    String name;
    List<Object> text;
    List<Object> media;
    String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Partner(String url, String name, List<Object> text, List<Object> media) {
        this.url = url;
        this.name = name;
        this.text = text;
        this.media = media;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getText() {
        return text;
    }

    public void setText(List<Object> text) {
        this.text = text;
    }

    public List<Object> getMedia() {
        return media;
    }

    public void setMedia(List<Object> media) {
        this.media = media;
    }
}
