
package com.muevaelvolante.racenet.model.post;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Highlight implements Serializable{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @SerializedName("button_text")
    @Expose
    private String buttonText;
    @SerializedName("path_url")
    @Expose
    private String pathUrl;
    @SerializedName("text")
    @Expose
    private List<Object> text = new ArrayList<Object>();
    @SerializedName("media")
    @Expose
    private List<Object> media = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The buttonText
     */
    public String getButtonText() {
        return buttonText;
    }

    /**
     * 
     * @param buttonText
     *     The button_text
     */
    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    /**
     * 
     * @return
     *     The pathUrl
     */
    public String getPathUrl() {
        return pathUrl;
    }

    /**
     * 
     * @param pathUrl
     *     The path_url
     */
    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    /**
     * 
     * @return
     *     The text
     */
    public List<Object> getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(List<Object> text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The media
     */
    public List<Object> getMedia() {
        return media;
    }

    /**
     * 
     * @param media
     *     The media
     */
    public void setMedia(List<Object> media) {
        this.media = media;
    }

}
