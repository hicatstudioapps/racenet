
package com.muevaelvolante.racenet.model.race_agenda;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Agenda implements Serializable{

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("no-events-today")
    @Expose
    private String no_events_today;
    @SerializedName("filters")
    @Expose
    private List<Filter> filters = null;
    @SerializedName("bigger_font")
    @Expose
    private String bigger_font;

    public String getBigger_font() {
        return bigger_font;
    }

    public void setBigger_font(String bigger_font) {
        this.bigger_font = bigger_font;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public String getNo_events_today() {
        return no_events_today;
    }

    public void setNo_events_today(String no_events_today) {
        this.no_events_today = no_events_today;
    }
}
