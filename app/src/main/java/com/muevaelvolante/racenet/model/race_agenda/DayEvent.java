package com.muevaelvolante.racenet.model.race_agenda;

import java.util.Date;

/**
 * Created by jorgedavid on 17/4/17.
 */

public class DayEvent {
    private Date date;
    private int isEvent=0;//0 vacio,1 no event,2 evento
    private int adapterPosition;

    public DayEvent(Date date, int isEvent) {
        this.date = date;
        this.isEvent = isEvent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int isEvent() {
        return isEvent;
    }

    public void setEvent(int event) {
        isEvent = event;
    }

    public void setAdapterPosition(int adapterPosition) {
        this.adapterPosition = adapterPosition;
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }
}
