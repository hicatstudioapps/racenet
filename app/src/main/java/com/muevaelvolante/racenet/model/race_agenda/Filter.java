
package com.muevaelvolante.racenet.model.race_agenda;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Filter implements Serializable{

    @SerializedName("filter")
    @Expose
    private String filter;

    public String getFilter() {
        return filter.toLowerCase();
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Filter(String filter) {
        this.filter = filter;
    }


}
