
package com.muevaelvolante.racenet.model.race_agenda;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filter_ {

    @SerializedName("filter")
    @Expose
    private String filter;

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

}
