
package com.muevaelvolante.racenet.model.race_agenda;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Medium {

    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("title")
    @Expose
    private String title;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
