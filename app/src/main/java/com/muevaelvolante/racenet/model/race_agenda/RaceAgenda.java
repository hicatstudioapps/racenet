
package com.muevaelvolante.racenet.model.race_agenda;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RaceAgenda {

    @SerializedName("agenda")
    @Expose
    private List<Agenda> agenda = null;
    @SerializedName("venue map")
    @Expose
    private List<VenueMap> venueMap = null;
    @SerializedName("filters")
    @Expose
    private List<Filter_> filters = null;

    public List<Agenda> getAgenda() {
        return agenda;
    }

    public void setAgenda(List<Agenda> agenda) {
        this.agenda = agenda;
    }

    public List<VenueMap> getVenueMap() {
        return venueMap;
    }

    public void setVenueMap(List<VenueMap> venueMap) {
        this.venueMap = venueMap;
    }

    public List<Filter_> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter_> filters) {
        this.filters = filters;
    }

}
