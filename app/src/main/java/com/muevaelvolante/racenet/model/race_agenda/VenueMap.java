
package com.muevaelvolante.racenet.model.race_agenda;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VenueMap {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("media")
    @Expose
    private List<Medium> media = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Medium> getMedia() {
        return media;
    }

    public void setMedia(List<Medium> media) {
        this.media = media;
    }

}
