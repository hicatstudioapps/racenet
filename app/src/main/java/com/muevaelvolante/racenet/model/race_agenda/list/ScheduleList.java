
package com.muevaelvolante.racenet.model.race_agenda.list;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScheduleList {

    @SerializedName("agenda")
    @Expose
    private List<Agenda> agenda = null;

    public List<Agenda> getAgenda() {
        return agenda;
    }

    public void setAgenda(List<Agenda> agenda) {
        this.agenda = agenda;
    }

}
