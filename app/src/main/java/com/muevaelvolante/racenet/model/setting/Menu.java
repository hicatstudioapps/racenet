
package com.muevaelvolante.racenet.model.setting;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("org.jsonschema2pojo")
public class Menu implements Serializable{

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("lastupdated")
    @Expose
    private String lastupdated;

    /**
     * 
     * @return
     *     The iD
     */
    public String getID() {
        return iD;
    }

    /**
     * 
     * @param iD
     *     The ID
     */
    public void setID(String iD) {
        this.iD = iD;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The Text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The uRL
     */
    public String getURL() {
        return uRL;
    }

    /**
     * 
     * @param uRL
     *     The URL
     */
    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    /**
     * 
     * @return
     *     The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon
     *     The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 
     * @return
     *     The lastupdated
     */
    public String getLastupdated() {
        return lastupdated;
    }

    /**
     * 
     * @param lastupdated
     *     The lastupdated
     */
    public void setLastupdated(String lastupdated) {
        this.lastupdated = lastupdated;
    }

}
