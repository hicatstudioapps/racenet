
package com.muevaelvolante.racenet.model.setting;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.muevaelvolante.racenet.model.notification.*;

@Generated("org.jsonschema2pojo")
public class Setting {

    @SerializedName("menu")
    @Expose
    private ArrayList<Menu> menu = new ArrayList<Menu>();
    @SerializedName("push-notifications")
    @Expose
    private List<com.muevaelvolante.racenet.model.notification.PushNotification> pushNotifications = new ArrayList<>();

    /**
     * 
     * @return
     *     The menu
     */
    public ArrayList<Menu> getMenu() {
        return menu;
    }

    /**
     * 
     * @param menu
     *     The menu
     */
    public void setMenu(ArrayList<Menu> menu) {
        this.menu = menu;
    }

    /**
     * 
     * @return
     *     The pushNotifications
     */
    public List<com.muevaelvolante.racenet.model.notification.PushNotification> getPushNotifications() {
        return pushNotifications;
    }

    /**
     * 
     * @param pushNotifications
     *     The push-notifications
     */
    public void setPushNotifications(List<com.muevaelvolante.racenet.model.notification.PushNotification> pushNotifications) {
        this.pushNotifications = pushNotifications;
    }

    public String getMenuUrl(String menuItem){
        for (Menu menu :
                getMenu()) {
            if (menu.getID().toLowerCase().equals(menuItem))
                return menu.getURL();
        }
        return null;
    }

    public Menu getMenuItemPosition(String menuItem){
        for (int i=0;i< getMenu().size(); i++) {
            String id=getMenu().get(i).getID().toLowerCase();
            if (getMenu().get(i).getID().toLowerCase().equals(menuItem.toLowerCase()))
              return  getMenu().get(i);
        }
        return null;
    }
}
