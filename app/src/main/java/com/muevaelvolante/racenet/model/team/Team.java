
package com.muevaelvolante.racenet.model.team;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Team {

    @SerializedName("teams")
    @Expose
    private List<Team_> teams = new ArrayList<Team_>();

    /**
     * 
     * @return
     *     The teams
     */
    public List<Team_> getTeams() {
        return teams;
    }

    /**
     * 
     * @param teams
     *     The teams
     */
    public void setTeams(List<Team_> teams) {
        this.teams = teams;
    }

}
