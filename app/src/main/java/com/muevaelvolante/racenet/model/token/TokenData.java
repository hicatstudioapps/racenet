
package com.muevaelvolante.racenet.model.token;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenData {

    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("device_platform")
    @Expose
    private String devicePlatform;
    @SerializedName("device_model")
    @Expose
    private String deviceModel;
    @SerializedName("device_push_type")
    @Expose
    private String devicePushType;
    @SerializedName("device_lang")
    @Expose
    private String deviceLang;
    @SerializedName("device_time_zone")
    @Expose
    private String deviceTimeZone;
    @SerializedName("app_identifier")
    @Expose
    private String appIdentifier;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("push_message_id")
    @Expose
    private String push_message_id;

    public String getPush_message_id() {
        return push_message_id;
    }

    public void setPush_message_id(String push_message_id) {
        this.push_message_id = push_message_id;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDevicePushType() {
        return devicePushType;
    }

    public void setDevicePushType(String devicePushType) {
        this.devicePushType = devicePushType;
    }

    public String getDeviceLang() {
        return deviceLang;
    }

    public void setDeviceLang(String deviceLang) {
        this.deviceLang = deviceLang;
    }

    public String getDeviceTimeZone() {
        return deviceTimeZone;
    }

    public void setDeviceTimeZone(String deviceTimeZone) {
        this.deviceTimeZone = deviceTimeZone;
    }

    public String getAppIdentifier() {
        return appIdentifier;
    }

    public void setAppIdentifier(String appIdentifier) {
        this.appIdentifier = appIdentifier;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

}
