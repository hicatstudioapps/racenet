
package com.muevaelvolante.racenet.model.who;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Category {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("subcategories")
    @Expose
    private List<Subcategory> subcategories = new ArrayList<Subcategory>();

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The subcategories
     */
    public List<Subcategory> getSubcategories() {
        return subcategories;
    }

    /**
     * 
     * @param subcategories
     *     The subcategories
     */
    public void setSubcategories(List<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

}
