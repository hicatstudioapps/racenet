
package com.muevaelvolante.racenet.model.who;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("org.jsonschema2pojo")
public class Media implements Serializable{

    @SerializedName("lo")
    @Expose
    private String lo;
    @SerializedName("med")
    @Expose
    private String med;
    @SerializedName("hi")
    @Expose
    private String hi;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    /**
     * 
     * @return
     *     The lo
     */
    public String getLo() {
        return lo;
    }

    /**
     * 
     * @param lo
     *     The lo
     */
    public void setLo(String lo) {
        this.lo = lo;
    }

    /**
     * 
     * @return
     *     The med
     */
    public String getMed() {
        return med;
    }

    /**
     * 
     * @param med
     *     The med
     */
    public void setMed(String med) {
        this.med = med;
    }

    /**
     * 
     * @return
     *     The hi
     */
    public String getHi() {
        return hi;
    }

    /**
     * 
     * @param hi
     *     The hi
     */
    public void setHi(String hi) {
        this.hi = hi;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The createdAt
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
