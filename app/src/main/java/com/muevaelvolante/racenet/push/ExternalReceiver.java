package com.muevaelvolante.racenet.push;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmReceiver;


public class ExternalReceiver extends GcmReceiver {

    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"Llego un push",Toast.LENGTH_LONG).show();
        if(intent!=null){
            Intent noty= new Intent(context,NotifyIntentService.class);
            noty.putExtras(intent.getExtras());
            startWakefulService(context,noty);
          }
    }
}

