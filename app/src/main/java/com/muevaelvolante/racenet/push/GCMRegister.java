package com.muevaelvolante.racenet.push;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.muevaelvolante.racenet.BuildConfig;
import com.muevaelvolante.racenet.Constants;
import com.muevaelvolante.racenet.interfaces.ISettings;
import com.muevaelvolante.racenet.manager.ManagerLanguages;
import com.muevaelvolante.racenet.model.token.TokenData;
import com.muevaelvolante.racenet.service.ServiceGenerator;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by jorgedavid on 25/4/17.
 */

public class GCMRegister extends AsyncTask<Void,Void,Void>{
    private GoogleCloudMessaging gcm;
        public static SharedPreferences savedValues;
        private AmazonSNSClient sns;
        Context context;
        String [] chanels;

        public GCMRegister(Context context) {
            this.context = context;
            gcm = GoogleCloudMessaging.getInstance(this.context);
        }

        private void register(final Context context) {
            String token;
            try {
                token = gcm.register("591052719952");
                SharedPreferences snspf= context.getSharedPreferences("sns",context.MODE_PRIVATE);
                    snspf.edit().putString("token",token).commit();
                sendRegistrationToServer(token);
                Log.i("registrationId", token);
            }
            catch (Exception e) {
                Log.i("Registration Error", e.getMessage());
            }
        }

        public IBinder onBind(Intent arg0) {
            return null;
        }

        @Override
        protected Void doInBackground(Void... params) {
            register(context);
            return null;
        }

    public static void sendRegistrationToServer(String refreshedToken) {
        String url="";
        TokenData tokenData= new TokenData();
        tokenData.setAppIdentifier("com.volvooceanrace.vor2014");
        tokenData.setAppVersion(BuildConfig.VERSION_CODE+"");
        tokenData.setDeviceLang(ManagerLanguages.getLang());
        tokenData.setDeviceToken(refreshedToken);
        tokenData.setDeviceModel(Build.MODEL);
        tokenData.setDevicePlatform("android");
        if(Constants.dev){
            url=" https://3sxzfrj9ti.execute-api.us-east-1.amazonaws.com/dev/mobile/devices";
        }else
            url="https://o6u8zm036e.execute-api.us-east-1.amazonaws.com/prod/mobile/devices";
        String finalUrl = url;
        Observable.create(subscriber -> {
            ISettings service= ServiceGenerator.createService(ISettings.class);
            String json= new Gson().toJson(tokenData,TokenData.class);
            Call<ResponseBody> call= service.sendToken(finalUrl,tokenData);
            try {
                Response<ResponseBody> body=   call.execute();
              //  String result= body.body().string();
                if(body.isSuccessful())
                    Log.d("DEVICE INFO SEND", "OK");
                else
                    Log.d("DEVICE INFO SEND", "FAIL");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
e.printStackTrace();
                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });
    }
}
