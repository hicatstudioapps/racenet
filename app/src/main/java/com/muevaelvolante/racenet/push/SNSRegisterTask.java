package com.muevaelvolante.racenet.push;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.google.android.gms.gcm.GoogleCloudMessaging;

@SuppressWarnings("WrongConstant")
public class SNSRegisterTask extends AsyncTask<Void,Void,Void>{

    private GoogleCloudMessaging gcm;
    public static SharedPreferences savedValues;
    private AmazonSNSClient sns;
    Context context;
    String [] chanels;

    public SNSRegisterTask(Context context,String [] chanels) {
        this.context = context;
        gcm = GoogleCloudMessaging.getInstance(this.context);
        sns = new AmazonSNSClient(new BasicAWSCredentials( "AKIAIBEAB2ZGCHDMRVIQ", "mLzlIuLqlAHACz2UtZmh2ma5zvzHPNeKBca5wcDw" ),
                new ClientConfiguration().withMaxConnections(100)
                        .withConnectionTimeout(120 * 1000)
                        .withSocketTimeout(10000)
                        .withMaxErrorRetry(15));
        sns.setRegion(Region.getRegion(Regions.US_EAST_1));
        this.chanels=chanels;
    }

    private void register(final Context context) {
                String token;
                try {
                    token = gcm.register("591052719952");
                    SharedPreferences snspf= context.getSharedPreferences("sns",context.MODE_PRIVATE);
                    if(TextUtils.isEmpty(snspf.getString("token",""))){
                        snspf.edit().putString("token",token).commit();
                    }
                    Log.i("registrationId", token);
                    AmazonSNSClientWrapper wrapper= new AmazonSNSClientWrapper(sns,context);
                    wrapper.registerChanels(token,chanels);
                } 
                catch (Exception e) {
                    Log.i("Registration Error", e.getMessage());
                }
                    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    protected Void doInBackground(Void... params) {
        register(context);
        return null;
    }
}