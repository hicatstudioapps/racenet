package com.muevaelvolante.racenet.service;

import com.muevaelvolante.racenet.Aplication;
import com.muevaelvolante.racenet.commons.Constants;
import com.muevaelvolante.racenet.util.Utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *  Developed by Mueva el Volante SL / Leo Pérez on 22/03/2016.
 *  Copyright © 2017 Volvo Ocean Race S.L.U. All rights reserved.
 *  https://racenetapp.s3.amazonaws.com/html/en/app/settings_test_tirar.json
 *  https://racenetapp.s3.amazonaws.com/html/en/app/racenet/
 */

public class ServiceGenerator  {
    public static final String BASE_URL = "https://racenetapp.s3.amazonaws.com/html/en/app/";
    public static final String BASE_API_SEARCH_CATEGORY_URL = "http://volvo-racenet.qiupreview.com/en/app/racenet/search/category.json?category=";
    public static final String BASE_API_SEARCH_TEXT_URL = "http://volvo-racenet.qiupreview.com/en/app/racenet/search/text.json?txt=";
//    public static final String BASE_URL = "http://192.168.101.1:8081/race/";

             private static Retrofit.Builder builder =
             new Retrofit.Builder()
             .baseUrl(BASE_URL)
                     .addConverterFactory(GsonConverterFactory.create());
             private static OkHttpClient httpClient =
             new OkHttpClient.Builder().build();

             public static <S> S createService(Class<S> serviceClass) {
         builder.client(httpClient);
         Retrofit retrofit = builder.build();
         return retrofit.create(serviceClass);
         }
}
